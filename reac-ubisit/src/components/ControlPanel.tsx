import React, { useRef } from 'react';
import { FaPlus, FaTrash, FaSave, FaRedo, FaRandom, FaDesktop, FaUser, FaFileExport, FaFileImport, FaUserMinus, FaFileAlt } from 'react-icons/fa';
import { Mode, Student } from '../types/models';
import StudentList from './StudentList';
import { Button } from './ui/button';
import { Card, CardContent, CardHeader } from './ui/card';
import { exportToJson, importFromJson } from '../utils/fileUtils';
import { useLanguage } from '../contexts/LanguageContext';
import LanguageSwitcher from './LanguageSwitcher';

interface ControlPanelProps {
  onAddDesk: () => void;
  onClearAll: () => void;
  onSaveLayout?: () => void;
  onResetLayout?: () => void;
  onRandomizeStudents: () => void;
  onRemoveAllStudents: () => void;
  onImportData: (data: { desks: any[], students: any[] }) => void;
  onImportStudentList: (students: string[]) => void;
  deskCount: number;
  mode: Mode;
  onModeChange: (mode: Mode) => void;
  students: Student[];
  assignedStudentIds: string[];
  onAddStudent: (name: string) => void;
  onRemoveStudent: (id: string) => void;
  onDragStudentStart: (student: Student) => void;
  onRemoveDesk?: (id: string) => void;
}

const ControlPanel: React.FC<ControlPanelProps> = ({ 
  onAddDesk, 
  onClearAll,
  onSaveLayout,
  onResetLayout,
  onRandomizeStudents,
  onRemoveAllStudents,
  onImportData,
  onImportStudentList,
  deskCount,
  mode,
  onModeChange,
  students,
  assignedStudentIds,
  onAddStudent,
  onRemoveStudent,
  onDragStudentStart,
  onRemoveDesk
}) => {
  const fileInputRef = useRef<HTMLInputElement>(null);
  const studentListInputRef = useRef<HTMLInputElement>(null);
  const { t } = useLanguage();

  const handleImportClick = () => {
    fileInputRef.current?.click();
  };

  const handleFileChange = async (e: React.ChangeEvent<HTMLInputElement>) => {
    const file = e.target.files?.[0];
    if (!file) return;

    try {
      const data = await importFromJson(file);
      onImportData(data);
      // Reset the input so the same file can be selected again
      e.target.value = '';
    } catch (error) {
      console.error('Error importing file:', error);
      alert('Error importing file. Please check the file format.');
    }
  };

  const handleExport = () => {
    exportToJson([], students); // The actual desks will be passed from App.tsx
  };

  const handleImportStudentListClick = () => {
    studentListInputRef.current?.click();
  };

  const handleStudentListFileChange = async (e: React.ChangeEvent<HTMLInputElement>) => {
    const file = e.target.files?.[0];
    if (!file) return;

    try {
      const reader = new FileReader();
      
      reader.onload = (event) => {
        const content = event.target?.result as string;
        let studentNames: string[] = [];
        
        if (file.name.endsWith('.csv')) {
          // Parse CSV file (simple implementation, assumes one student per line)
          studentNames = content.split('\n')
            .map(line => line.split(',')[0]?.trim()) // Take first column
            .filter(name => name && name.length > 0);
        } else {
          // Parse TXT file (one student per line)
          studentNames = content.split('\n')
            .map(line => line.trim())
            .filter(name => name && name.length > 0);
        }
        
        onImportStudentList(studentNames);
      };
      
      reader.readAsText(file);
      
      // Reset the input so the same file can be selected again
      e.target.value = '';
    } catch (error) {
      console.error('Error importing student list:', error);
      alert('Error importing student list. Please check the file format.');
    }
  };

  // Filter out assigned students from the list
  const availableStudents = students.filter(student => !assignedStudentIds.includes(student.id));

  return (
    <Card className="fixed top-4 right-4 w-72 max-h-[calc(100vh-2rem)] overflow-y-auto shadow-sm border-slate-50 bg-white/95 backdrop-blur-sm z-10 rounded-xl">
      <CardHeader className="p-4 flex flex-row items-center justify-between border-b border-slate-50">
        <h2 className="text-xl font-medium text-slate-600">{t('appTitle')}</h2>
        <LanguageSwitcher />
      </CardHeader>
      
      <CardContent className="p-4 pt-3 space-y-3">
        {/* Mode Switcher with Counters */}
        <Card className="border-none shadow-none bg-transparent">
          <CardContent className="p-0">
            <div className="flex rounded-lg overflow-hidden border border-slate-100">
              <Button 
                variant={mode === 'desk' ? 'primary' : 'secondary'}
                className={`flex-1 py-2 px-3 flex items-center justify-center gap-1 rounded-r-none ${mode === 'desk' ? 'bg-blue-400 hover:bg-blue-500' : 'bg-slate-50 hover:bg-slate-100 text-slate-600'}`}
                onClick={() => onModeChange('desk')}
              >
                <FaDesktop size={14} />
                <span>{t('desks')} <span className="font-medium ml-1">{deskCount}</span></span>
              </Button>
              <Button 
                variant={mode === 'student' ? 'primary' : 'secondary'}
                className={`flex-1 py-2 px-3 flex items-center justify-center gap-1 rounded-l-none ${mode === 'student' ? 'bg-blue-400 hover:bg-blue-500' : 'bg-slate-50 hover:bg-slate-100 text-slate-600'}`}
                onClick={() => onModeChange('student')}
              >
                <FaUser size={14} />
                <span>
                  {t('students')} 
                  <span className="font-medium ml-1">{students.length}</span>
                  {assignedStudentIds.length > 0 && mode === 'student' && (
                    <span className="text-blue-100 text-xs ml-1">
                      ({assignedStudentIds.length} {t('assigned')})
                    </span>
                  )}
                </span>
              </Button>
            </div>
          </CardContent>
        </Card>
        
        {mode === 'desk' ? (
          <div className="space-y-3">
            <Button 
              variant="primary"
              className="w-full flex items-center justify-center gap-2 bg-blue-400 hover:bg-blue-500"
              onClick={onAddDesk}
            >
              <FaPlus className="text-sm" />
              <span>{t('addDesk')}</span>
            </Button>

            <div className="flex gap-2">
              <Button 
                variant="destructive"
                className="flex-1 flex items-center justify-center gap-2 bg-red-400 hover:bg-red-500"
                onClick={onClearAll}
              >
                <FaTrash className="text-sm" />
                <span>{t('clearAll')}</span>
              </Button>
              
              {onSaveLayout && (
                <Button 
                  variant="secondary"
                  className="flex-1 flex items-center justify-center gap-2 bg-slate-50 hover:bg-slate-100 text-slate-600"
                  onClick={onSaveLayout}
                >
                  <FaSave className="text-sm" />
                  <span>{t('saveLayout')}</span>
                </Button>
              )}
            </div>
          </div>
        ) : (
          <div className="space-y-3">
            <StudentList 
              students={availableStudents}
              onAddStudent={onAddStudent}
              onRemoveStudent={onRemoveStudent}
              onDragStart={onDragStudentStart}
            />
            
            <div className="flex gap-2">
              <Button 
                variant="primary"
                className="flex-1 flex items-center justify-center gap-2 bg-blue-400 hover:bg-blue-500"
                disabled={students.length === 0 || deskCount === 0}
                onClick={onRandomizeStudents}
              >
                <FaRandom className="text-sm" />
                <span>{t('randomizeSeats')}</span>
              </Button>
              
              <Button 
                variant="secondary"
                className="flex-1 flex items-center justify-center gap-2 bg-slate-50 hover:bg-slate-100 text-slate-600"
                disabled={assignedStudentIds.length === 0}
                onClick={onRemoveAllStudents}
              >
                <FaUserMinus className="text-sm" />
                <span>{t('removeAllStudents')}</span>
              </Button>
            </div>
          </div>
        )}
        
        {/* Import/Export */}
        <div className="pt-2 border-t border-slate-50">
          <div className="flex gap-2">
            <Button 
              variant="outline"
              className="flex-1 flex items-center justify-center gap-2 border-slate-100 text-slate-600 hover:bg-slate-50"
              onClick={handleImportClick}
            >
              <FaFileImport className="text-sm" />
              <span>{t('importLayout')}</span>
            </Button>
            <input 
              type="file" 
              ref={fileInputRef} 
              onChange={handleFileChange} 
              accept=".json" 
              className="hidden" 
            />
            <Button 
              variant="outline"
              className="flex-1 flex items-center justify-center gap-2 border-slate-100 text-slate-600 hover:bg-slate-50"
              onClick={handleExport}
            >
              <FaFileExport className="text-sm" />
              <span>{t('exportLayout')}</span>
            </Button>
          </div>
          
          {mode === 'student' && (
            <Button 
              variant="outline"
              className="w-full mt-2 flex items-center justify-center gap-2 border-slate-100 text-slate-600 hover:bg-slate-50"
              onClick={handleImportStudentListClick}
            >
              <FaFileAlt className="text-sm" />
              <span>{t('importStudentList')}</span>
            </Button>
          )}
          <input 
            type="file" 
            ref={studentListInputRef} 
            onChange={handleStudentListFileChange} 
            accept=".txt,.csv" 
            className="hidden" 
          />
        </div>
        
        <div className="text-xs text-slate-400 pt-2 border-t border-slate-50">
          <p>{t('dragDesksInfo')}</p>
          {mode === 'student' && (
            <p className="mt-1">{t('dragStudentsInfo')}</p>
          )}
        </div>
      </CardContent>
    </Card>
  );
};

export default ControlPanel; 