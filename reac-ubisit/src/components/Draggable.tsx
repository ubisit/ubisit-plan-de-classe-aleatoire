import React, { useState, useRef, useEffect, ReactNode } from 'react';

interface DraggableProps {
  children: ReactNode;
  initialPosition?: { x: number; y: number };
  onPositionChange?: (position: { x: number; y: number }) => void;
  bounds?: 'parent' | null;
  onDragStart?: () => void;
  onDragEnd?: () => void;
  disabled?: boolean;
}

const Draggable: React.FC<DraggableProps> = ({
  children,
  initialPosition = { x: 0, y: 0 },
  onPositionChange,
  bounds = null,
  onDragStart,
  onDragEnd,
  disabled = false
}) => {
  const [position, setPosition] = useState(initialPosition);
  const [isDragging, setIsDragging] = useState(false);
  const elementRef = useRef<HTMLDivElement>(null);
  const parentRef = useRef<HTMLDivElement>(null);
  
  // Store the initial mouse position and element position when dragging starts
  const dragStartRef = useRef({
    mouseX: 0,
    mouseY: 0,
    elementX: 0,
    elementY: 0
  });

  useEffect(() => {
    if (elementRef.current && bounds === 'parent') {
      parentRef.current = elementRef.current.parentElement as HTMLDivElement;
    }
  }, [bounds]);

  const handleMouseDown = (e: React.MouseEvent) => {
    if (disabled) return;
    
    // Check if the click is on an element with the handle class or one of its children
    if (e.target instanceof HTMLElement && (e.target.classList.contains('handle') || e.target.closest('.handle'))) {
      e.preventDefault();
      e.stopPropagation(); // Prevent event bubbling
      
      // Store the initial mouse position and element position
      dragStartRef.current = {
        mouseX: e.clientX,
        mouseY: e.clientY,
        elementX: position.x,
        elementY: position.y
      };
      
      setIsDragging(true);
      
      if (onDragStart) {
        onDragStart();
      }
    }
  };

  const handleMouseMove = (e: MouseEvent) => {
    if (!isDragging || disabled) return;
    
    // Calculate the new position based on the mouse movement
    const deltaX = e.clientX - dragStartRef.current.mouseX;
    const deltaY = e.clientY - dragStartRef.current.mouseY;
    
    let newX = dragStartRef.current.elementX + deltaX;
    let newY = dragStartRef.current.elementY + deltaY;
    
    // Apply bounds if needed
    if (bounds === 'parent' && parentRef.current && elementRef.current) {
      const parentRect = parentRef.current.getBoundingClientRect();
      const elementRect = elementRef.current.getBoundingClientRect();
      
      // Ensure the element stays within the parent bounds
      const minX = 0;
      const minY = 0;
      const maxX = parentRect.width - elementRect.width;
      const maxY = parentRect.height - elementRect.height;
      
      newX = Math.max(minX, Math.min(newX, maxX));
      newY = Math.max(minY, Math.min(newY, maxY));
    }
    
    const newPosition = { x: newX, y: newY };
    setPosition(newPosition);
    
    if (onPositionChange) {
      onPositionChange(newPosition);
    }
  };

  const handleMouseUp = () => {
    if (isDragging) {
      setIsDragging(false);
      if (onDragEnd) {
        onDragEnd();
      }
    }
  };

  useEffect(() => {
    if (isDragging && !disabled) {
      window.addEventListener('mousemove', handleMouseMove);
      window.addEventListener('mouseup', handleMouseUp);
    } else {
      window.removeEventListener('mousemove', handleMouseMove);
      window.removeEventListener('mouseup', handleMouseUp);
    }

    return () => {
      window.removeEventListener('mousemove', handleMouseMove);
      window.removeEventListener('mouseup', handleMouseUp);
    };
  }, [isDragging, disabled]);

  // Update position when initialPosition changes
  useEffect(() => {
    setPosition(initialPosition);
  }, [initialPosition]);

  return (
    <div
      ref={elementRef}
      style={{
        position: 'absolute',
        left: `${position.x}px`,
        top: `${position.y}px`,
        cursor: disabled ? 'default' : (isDragging ? 'grabbing' : 'grab'),
        touchAction: 'none',
        userSelect: 'none' // Prevent text selection during drag
      }}
      onMouseDown={handleMouseDown}
    >
      {children}
    </div>
  );
};

export default Draggable; 