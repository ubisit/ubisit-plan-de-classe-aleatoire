import React, { useState } from 'react';
import { FaPlus, FaTrash, FaUser } from 'react-icons/fa';
import { Student } from '../types/models';
import { Button } from './ui/button';
import { Input } from './ui/input';
import { useLanguage } from '../contexts/LanguageContext';

interface StudentListProps {
  students: Student[];
  onAddStudent: (name: string) => void;
  onRemoveStudent: (id: string) => void;
  onDragStart: (student: Student) => void;
}

const StudentList: React.FC<StudentListProps> = ({ 
  students, 
  onAddStudent, 
  onRemoveStudent,
  onDragStart
}) => {
  const [newStudentName, setNewStudentName] = useState('');
  const { t } = useLanguage();

  const handleAddStudent = () => {
    if (newStudentName.trim()) {
      onAddStudent(newStudentName.trim());
      setNewStudentName('');
    }
  };

  const handleKeyDown = (e: React.KeyboardEvent) => {
    if (e.key === 'Enter') {
      handleAddStudent();
    }
  };

  const handleDragStart = (e: React.DragEvent, student: Student) => {
    e.dataTransfer.setData('student', JSON.stringify(student));
    onDragStart(student);
  };

  return (
    <div className="space-y-3">
      <div className="flex items-center gap-2">
        <Input
          type="text"
          value={newStudentName}
          onChange={(e) => setNewStudentName(e.target.value)}
          onKeyDown={handleKeyDown}
          placeholder={t('addStudent')}
          className="flex-1 border-slate-100 focus:border-blue-300 focus:ring-blue-300"
        />
        <Button
          variant="primary"
          size="icon"
          onClick={handleAddStudent}
          disabled={!newStudentName.trim()}
          className="bg-blue-400 hover:bg-blue-500"
        >
          <FaPlus />
        </Button>
      </div>

      <div className="max-h-60 overflow-y-auto rounded-md bg-white">
        {students.length === 0 ? (
          <div className="p-3 text-center text-slate-400 border border-slate-50 rounded-md">{t('noStudentsAvailable')}</div>
        ) : (
          <ul className="divide-y divide-slate-50 border border-slate-50 rounded-md">
            {students.map((student) => (
              <li 
                key={student.id}
                className="p-2 flex items-center justify-between hover:bg-slate-50 transition-colors"
                draggable
                onDragStart={(e) => handleDragStart(e, student)}
              >
                <div className="flex items-center gap-2">
                  <FaUser className="text-blue-400" />
                  <span className="text-slate-600">{student.name}</span>
                </div>
                <Button
                  variant="ghost"
                  size="sm"
                  className="h-7 w-7 p-0 text-red-400 hover:text-red-500 hover:bg-red-50 rounded-full"
                  onClick={() => onRemoveStudent(student.id)}
                >
                  <FaTrash size={12} />
                </Button>
              </li>
            ))}
          </ul>
        )}
      </div>
    </div>
  );
};

export default StudentList; 