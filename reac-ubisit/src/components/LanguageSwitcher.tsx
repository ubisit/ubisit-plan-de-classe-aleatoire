import React from 'react';
import { Button } from './ui/button';
import { useLanguage, Language } from '../contexts/LanguageContext';

const LanguageSwitcher: React.FC = () => {
  const { language, setLanguage } = useLanguage();

  const toggleLanguage = () => {
    setLanguage(language === 'fr' ? 'en' : 'fr');
  };

  return (
    <Button
      variant="outline"
      size="sm"
      onClick={toggleLanguage}
      className="rounded-full w-10 h-8 flex items-center justify-center text-xs font-medium transition-all duration-200 border-slate-100 text-slate-500 hover:bg-slate-50 hover:text-blue-400"
    >
      {language === 'fr' ? 'EN' : 'FR'}
    </Button>
  );
};

export default LanguageSwitcher; 