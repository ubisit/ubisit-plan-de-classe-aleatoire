import React from 'react';
import { FaUser } from 'react-icons/fa';
import { Student } from '../types/models';
import { useLanguage } from '../contexts/LanguageContext';

interface StudentTagProps {
  deskId: string;
  student?: Student;
  deskNumber: number;
}

const StudentTag: React.FC<StudentTagProps> = ({ student, deskNumber }) => {
  const { t } = useLanguage();
  
  return (
    <div className="w-24 bg-white rounded-t-md p-1.5 pb-3 text-center border border-slate-100 border-b-0 shadow-sm transition-all">
      <div className="flex items-center justify-center gap-1">
        <FaUser className="text-blue-400 text-xs" />
        <span className="text-xs font-medium text-slate-600 truncate">
          {student ? student.name : `${t('desk')} ${deskNumber}`}
        </span>
      </div>
    </div>
  );
};

export default StudentTag; 