import React from 'react';
import { FaChalkboardTeacher } from 'react-icons/fa';

interface ClassroomProps {
  children: React.ReactNode;
  teacherPosition?: { x: number; y: number };
}

const Classroom: React.FC<ClassroomProps> = ({ children, teacherPosition = { x: window.innerWidth / 2 - 24, y: 50 } }) => {
  return (
    <div className="relative w-full h-full overflow-hidden">
      {/* Teacher's desk and board */}
      <div 
        className="absolute flex flex-col items-center z-10"
        style={{ 
          left: `${teacherPosition.x}px`, 
          top: `${teacherPosition.y}px`,
          transform: 'translateX(-50%)'
        }}
      >
        <div className="w-48 h-3 bg-slate-200 rounded-t-md"></div>
        <div className="w-48 h-16 bg-slate-100 flex items-center justify-center border border-slate-200 rounded-sm">
          <FaChalkboardTeacher className="text-slate-500 text-2xl" />
        </div>
        <div className="mt-4 w-24 h-12 bg-white rounded-md shadow-sm border border-slate-100 flex items-center justify-center">
          <div className="w-16 h-8 bg-slate-50 rounded-sm border border-slate-200"></div>
        </div>
      </div>
      
      {/* Classroom floor - just a container for the desks */}
      <div className="absolute inset-0 p-24">
        {children}
      </div>
    </div>
  );
};

export default Classroom; 