import React, { useEffect, useState } from 'react';
import { FaTimes } from 'react-icons/fa';
import Draggable from './Draggable';
import StudentTag from './StudentTag';
import { Student, DeskItem, Mode } from '../types/models';
import { Button } from './ui/button';
import { useLanguage } from '../contexts/LanguageContext';

interface DeskProps {
  id: string;
  index: number;
  initialPosition: { x: number; y: number };
  onPositionChange: (id: string, position: { x: number; y: number }) => void;
  student?: Student;
  otherDesks: DeskItem[];
  onDrop?: (e: React.DragEvent, deskId: string) => void;
  onDragOver?: (e: React.DragEvent) => void;
  onRemoveDesk?: (id: string) => void;
  mode: Mode;
  onStudentTagDrop?: (sourceDeskId: string, targetDeskId: string) => void;
}

const Desk: React.FC<DeskProps> = ({ 
  id, 
  index,
  initialPosition, 
  onPositionChange, 
  student,
  otherDesks,
  onDrop,
  onDragOver,
  onRemoveDesk,
  mode,
  onStudentTagDrop
}) => {
  const [position, setPosition] = useState(initialPosition);
  const [isDragging, setIsDragging] = useState(false);
  const [isHovered, setIsHovered] = useState(false);
  const { t } = useLanguage();

  // Handle position changes (without magnetic snapping)
  const handlePositionChange = (newPosition: { x: number; y: number }) => {
    // Ensure the position is not negative
    const validPosition = {
      x: Math.max(0, newPosition.x),
      y: Math.max(0, newPosition.y)
    };
    
    setPosition(validPosition);
    onPositionChange(id, validPosition);
  };

  // Handle student tag drag start
  const handleStudentTagDragStart = (e: React.DragEvent) => {
    if (mode !== 'student' || !student) return;
    
    e.dataTransfer.setData('sourceDeskId', id);
    e.dataTransfer.setData('studentData', JSON.stringify(student));
    e.dataTransfer.effectAllowed = 'move';
  };

  // Handle student tag drop
  const handleStudentTagDrop = (e: React.DragEvent) => {
    if (mode !== 'student') return;
    
    e.preventDefault();
    const sourceDeskId = e.dataTransfer.getData('sourceDeskId');
    
    if (sourceDeskId && sourceDeskId !== id && onStudentTagDrop) {
      onStudentTagDrop(sourceDeskId, id);
    }
  };

  // Update position when initialPosition changes
  useEffect(() => {
    setPosition(initialPosition);
  }, [initialPosition]);

  return (
    <Draggable 
      initialPosition={position}
      bounds="parent"
      onPositionChange={handlePositionChange}
      onDragStart={() => setIsDragging(true)}
      onDragEnd={() => setIsDragging(false)}
      disabled={mode === 'student'}
    >
      <div 
        className="cursor-move relative handle"
        onDragOver={(e) => {
          e.preventDefault();
          if (onDragOver) onDragOver(e);
        }}
        onDrop={(e) => {
          if (mode === 'student') {
            handleStudentTagDrop(e);
          } else if (onDrop) {
            onDrop(e, id);
          }
        }}
        onMouseEnter={() => setIsHovered(true)}
        onMouseLeave={() => setIsHovered(false)}
      >
        {/* Student Tag - Positioned to appear contained by the desk */}
        <div
          draggable={mode === 'student' && !!student}
          onDragStart={handleStudentTagDragStart}
          className={`absolute left-1/2 -translate-x-1/2 -top-10 z-10 ${mode === 'student' && !!student ? 'cursor-grab active:cursor-grabbing' : ''}`}
        >
          <StudentTag deskId={id} student={student} deskNumber={index + 1} />
        </div>
        
        <div className="w-24 h-16 bg-white rounded-md shadow-lg border border-slate-100 flex flex-col items-center justify-center">
          <div className="w-full h-full flex items-center justify-center">
            <div className="w-16 h-10 bg-slate-50 rounded-sm border border-slate-200"></div>
          </div>
          
          {/* Remove desk button - animated on hover in desk mode */}
          {mode === 'desk' && onRemoveDesk && (
            <Button
              variant="destructive"
              size="icon"
              className={`absolute -top-2 -right-2 h-6 w-6 rounded-full shadow-md transition-all duration-300 z-20 ${
                isHovered 
                  ? 'opacity-100 scale-100' 
                  : 'opacity-0 scale-75 pointer-events-none'
              }`}
              onClick={(e) => {
                e.stopPropagation();
                onRemoveDesk(id);
              }}
            >
              <FaTimes size={10} className="text-white" />
            </Button>
          )}
        </div>
      </div>
    </Draggable>
  );
};

export default Desk; 