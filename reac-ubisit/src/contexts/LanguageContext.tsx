import React, { createContext, useState, useContext, ReactNode } from 'react';

// Define available languages
export type Language = 'fr' | 'en';

// Define translations
export const translations = {
  fr: {
    appTitle: 'UbiSit',
    mode: 'Mode',
    desks: 'Bureaux',
    students: 'Élèves',
    data: 'Données',
    importLayout: 'Importer Disposition',
    exportLayout: 'Exporter Disposition',
    importStudentList: 'Importer Liste d\'Élèves',
    addDesk: 'Ajouter Bureau',
    layout: 'Disposition',
    saveLayout: 'Sauvegarder',
    resetLayout: 'Réinitialiser',
    clearAll: 'Tout Effacer',
    actions: 'Actions',
    randomizeSeats: 'Places Aléatoires',
    removeAllStudents: 'Retirer Tous les Élèves',
    addStudent: 'Ajouter Élève',
    noStudentsAvailable: 'Aucun élève disponible',
    dragDesksInfo: 'Glissez les bureaux pour les positionner dans la classe.',
    dragStudentsInfo: 'Glissez les élèves sur les bureaux pour attribuer des places.',
    assigned: 'assigné(s)',
    desk: 'Bureau',
  },
  en: {
    appTitle: 'UbiSit',
    mode: 'Mode',
    desks: 'Desks',
    students: 'Students',
    data: 'Data',
    importLayout: 'Import Layout',
    exportLayout: 'Export Layout',
    importStudentList: 'Import Student List',
    addDesk: 'Add Desk',
    layout: 'Layout',
    saveLayout: 'Save Layout',
    resetLayout: 'Reset Layout',
    clearAll: 'Clear All',
    actions: 'Actions',
    randomizeSeats: 'Randomize Seats',
    removeAllStudents: 'Remove All Students',
    addStudent: 'Add Student',
    noStudentsAvailable: 'No students available',
    dragDesksInfo: 'Drag desks to position them in the classroom.',
    dragStudentsInfo: 'Drag students onto desks to assign seats.',
    assigned: 'assigned',
    desk: 'Desk',
  }
};

// Create the context
interface LanguageContextType {
  language: Language;
  setLanguage: (language: Language) => void;
  t: (key: keyof typeof translations.en) => string;
}

const LanguageContext = createContext<LanguageContextType | undefined>(undefined);

// Create provider component
interface LanguageProviderProps {
  children: ReactNode;
}

export const LanguageProvider: React.FC<LanguageProviderProps> = ({ children }) => {
  const [language, setLanguage] = useState<Language>('fr');

  // Translation function
  const t = (key: keyof typeof translations.en): string => {
    return translations[language][key] || key;
  };

  return (
    <LanguageContext.Provider value={{ language, setLanguage, t }}>
      {children}
    </LanguageContext.Provider>
  );
};

// Custom hook to use the language context
export const useLanguage = (): LanguageContextType => {
  const context = useContext(LanguageContext);
  if (context === undefined) {
    throw new Error('useLanguage must be used within a LanguageProvider');
  }
  return context;
}; 