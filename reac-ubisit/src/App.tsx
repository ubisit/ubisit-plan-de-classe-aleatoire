import { useState, useEffect } from 'react'
import { v4 as uuidv4 } from 'uuid'
import ControlPanel from './components/ControlPanel'
import Classroom from './components/Classroom'
import Desk from './components/Desk'
import { DeskItem, Student, Mode } from './types/models'
import { findNextAvailablePosition } from './utils/deskUtils'
import { exportToJson } from './utils/fileUtils'
import { LanguageProvider } from './contexts/LanguageContext'

function App() {
  // State
  const [desks, setDesks] = useState<DeskItem[]>([])
  const [students, setStudents] = useState<Student[]>([])
  const [mode, setMode] = useState<Mode>('desk')
  const [draggingStudent, setDraggingStudent] = useState<Student | null>(null)
  const [teacherDeskPosition, setTeacherDeskPosition] = useState({ x: window.innerWidth / 2 - 24, y: 50 })

  // Get array of assigned student IDs
  const assignedStudentIds = desks
    .filter(desk => desk.studentId)
    .map(desk => desk.studentId as string)

  // Add a new desk
  const handleAddDesk = () => {
    const newPosition = findNextAvailablePosition(desks)
    
    const newDesk: DeskItem = {
      id: uuidv4(),
      position: newPosition
    }
    
    setDesks([...desks, newDesk])
    updateTeacherDeskPosition([...desks, newDesk])
  }

  // Remove a desk
  const handleRemoveDesk = (id: string) => {
    const updatedDesks = desks.filter(desk => desk.id !== id)
    setDesks(updatedDesks)
    updateTeacherDeskPosition(updatedDesks)
  }

  // Update desk position
  const handleDeskPositionChange = (id: string, position: { x: number; y: number }) => {
    const updatedDesks = desks.map(desk => 
      desk.id === id ? { ...desk, position } : desk
    )
    setDesks(updatedDesks)
    updateTeacherDeskPosition(updatedDesks)
  }

  // Update teacher desk position based on student desks
  const updateTeacherDeskPosition = (currentDesks: DeskItem[]) => {
    if (currentDesks.length === 0) {
      setTeacherDeskPosition({ x: window.innerWidth / 2 - 24, y: 50 })
      return
    }

    // Find the average position of all desks
    const totalX = currentDesks.reduce((sum, desk) => sum + desk.position.x, 0)
    const avgX = totalX / currentDesks.length

    // Find the minimum Y position (closest to the top)
    const minY = Math.min(...currentDesks.map(desk => desk.position.y))

    // Position the teacher's desk at the average X, but above all student desks
    // with a significant margin to avoid overlap
    const newTeacherX = avgX
    const newTeacherY = Math.max(50, minY - 150) // At least 150px above the topmost desk

    setTeacherDeskPosition({ x: newTeacherX, y: newTeacherY })
  }

  // Clear all desks
  const handleClearAll = () => {
    setDesks([])
    setTeacherDeskPosition({ x: window.innerWidth / 2 - 24, y: 50 })
  }

  // Add a new student
  const handleAddStudent = (name: string) => {
    const newStudent: Student = {
      id: uuidv4(),
      name
    }
    
    setStudents([...students, newStudent])
  }

  // Remove a student
  const handleRemoveStudent = (id: string) => {
    // Remove student from the list
    setStudents(students.filter(student => student.id !== id))
    
    // Also remove any assignments of this student to desks
    setDesks(desks.map(desk => 
      desk.studentId === id ? { ...desk, studentId: undefined } : desk
    ))
  }

  // Handle student drag start
  const handleDragStudentStart = (student: Student) => {
    setDraggingStudent(student)
  }

  // Handle drop on desk
  const handleDeskDrop = (e: React.DragEvent, deskId: string) => {
    e.preventDefault()
    
    if (!draggingStudent) return
    
    // Assign student to desk
    setDesks(desks.map(desk => 
      desk.id === deskId ? { ...desk, studentId: draggingStudent.id } : desk
    ))
    
    setDraggingStudent(null)
  }

  // Handle student tag exchange between desks
  const handleStudentTagDrop = (sourceDeskId: string, targetDeskId: string) => {
    // Find the source and target desks
    const sourceDesk = desks.find(desk => desk.id === sourceDeskId)
    const targetDesk = desks.find(desk => desk.id === targetDeskId)
    
    if (!sourceDesk || !targetDesk) return
    
    // Exchange student IDs
    const sourceStudentId = sourceDesk.studentId
    const targetStudentId = targetDesk.studentId
    
    setDesks(desks.map(desk => {
      if (desk.id === sourceDeskId) {
        return { ...desk, studentId: targetStudentId }
      } else if (desk.id === targetDeskId) {
        return { ...desk, studentId: sourceStudentId }
      }
      return desk
    }))
  }

  // Handle drag over desk
  const handleDeskDragOver = (e: React.DragEvent) => {
    e.preventDefault()
  }

  // Randomize student placement
  const handleRandomizeStudents = () => {
    if (desks.length === 0 || students.length === 0) return
    
    // Create a copy of the desks
    const desksToUpdate = [...desks]
    
    // Clear all student assignments
    desksToUpdate.forEach(desk => {
      desk.studentId = undefined
    })
    
    // Get all available students
    const availableStudents = [...students]
    
    // Shuffle the students
    for (let i = availableStudents.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1))
      const temp = availableStudents[i]
      availableStudents[i] = availableStudents[j]
      availableStudents[j] = temp
    }
    
    // Shuffle the desks to ensure random assignment
    const shuffledDeskIndices = Array.from({ length: desksToUpdate.length }, (_, i) => i)
    for (let i = shuffledDeskIndices.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1))
      const temp = shuffledDeskIndices[i]
      shuffledDeskIndices[i] = shuffledDeskIndices[j]
      shuffledDeskIndices[j] = temp
    }
    
    // Assign students to desks using the shuffled indices
    const maxAssignments = Math.min(availableStudents.length, desksToUpdate.length)
    for (let i = 0; i < maxAssignments; i++) {
      desksToUpdate[shuffledDeskIndices[i]].studentId = availableStudents[i].id
    }
    
    setDesks(desksToUpdate)
  }

  // Remove all students from seats
  const handleRemoveAllStudents = () => {
    setDesks(desks.map(desk => ({ ...desk, studentId: undefined })))
  }

  // Get student by ID
  const getStudentById = (id?: string): Student | undefined => {
    if (!id) return undefined
    return students.find(student => student.id === id)
  }

  // Handle classroom drop for student assignment
  const handleClassroomDrop = (e: React.DragEvent) => {
    e.preventDefault()
    setDraggingStudent(null)
  }

  // Handle import data
  const handleImportData = (data: { desks: DeskItem[], students: Student[] }) => {
    setDesks(data.desks)
    setStudents(data.students)
    updateTeacherDeskPosition(data.desks)
  }

  // Handle import student list
  const handleImportStudentList = (studentNames: string[]) => {
    const newStudents = studentNames.map(name => ({
      id: uuidv4(),
      name
    }))
    
    setStudents([...students, ...newStudents])
  }

  // Handle export data
  const handleExport = () => {
    exportToJson(desks, students)
  }

  // Update teacher desk position on window resize
  useEffect(() => {
    const handleResize = () => {
      updateTeacherDeskPosition(desks)
    }

    window.addEventListener('resize', handleResize)
    return () => window.removeEventListener('resize', handleResize)
  }, [desks])

  return (
    <LanguageProvider>
      <div className="flex h-screen w-screen overflow-hidden bg-white" 
        style={{
          backgroundImage: 'linear-gradient(to right, #f3f4f6 1px, transparent 1px), linear-gradient(to bottom, #f3f4f6 1px, transparent 1px)',
          backgroundSize: '20px 20px'
        }}
      >
        {/* Classroom */}
        <div 
          className="flex-1 relative"
          onDragOver={handleDeskDragOver}
          onDrop={handleClassroomDrop}
        >
          <Classroom teacherPosition={teacherDeskPosition}>
            {desks.map((desk, index) => (
              <div 
                key={desk.id}
                onDragOver={handleDeskDragOver}
                onDrop={(e) => handleDeskDrop(e, desk.id)}
              >
                <Desk 
                  id={desk.id}
                  index={index}
                  initialPosition={desk.position}
                  onPositionChange={handleDeskPositionChange}
                  student={getStudentById(desk.studentId)}
                  otherDesks={desks.filter(d => d.id !== desk.id)}
                  onDragOver={handleDeskDragOver}
                  onDrop={handleDeskDrop}
                  onRemoveDesk={handleRemoveDesk}
                  mode={mode}
                  onStudentTagDrop={handleStudentTagDrop}
                />
              </div>
            ))}
          </Classroom>
        </div>
        
        {/* Control Panel */}
        <ControlPanel 
          onAddDesk={handleAddDesk} 
          onClearAll={handleClearAll}
          onRandomizeStudents={handleRandomizeStudents}
          onRemoveAllStudents={handleRemoveAllStudents}
          onImportData={handleImportData}
          onImportStudentList={handleImportStudentList}
          deskCount={desks.length}
          mode={mode}
          onModeChange={setMode}
          students={students}
          assignedStudentIds={assignedStudentIds}
          onAddStudent={handleAddStudent}
          onRemoveStudent={handleRemoveStudent}
          onDragStudentStart={handleDragStudentStart}
          onRemoveDesk={handleRemoveDesk}
        />
      </div>
    </LanguageProvider>
  )
}

export default App
