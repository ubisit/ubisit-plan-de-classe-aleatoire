import { DeskItem, Student } from "../types/models";
import { v4 as uuidv4 } from 'uuid';

// Interface matching the required JSON format
interface ImportFormat {
  [key: string]: {
    left: string;
    top: string;
    student: string;
  };
}

// Convert desks and students to the required export format
export const convertToExportFormat = (desks: DeskItem[], students: Student[]): ImportFormat => {
  const result: ImportFormat = {};
  
  desks.forEach((desk, index) => {
    const student = students.find(s => s.id === desk.studentId);
    result[index.toString()] = {
      left: `${desk.position.x}px`,
      top: `${desk.position.y}px`,
      student: student ? student.name : `Place ${index + 1}`
    };
  });
  
  return result;
};

// Convert from import format to our app's format
export const convertFromImportFormat = (data: ImportFormat): { desks: DeskItem[], students: Student[] } => {
  const desks: DeskItem[] = [];
  const studentsMap = new Map<string, Student>();
  
  Object.entries(data).forEach(([key, value]) => {
    // Parse position values
    const x = parseInt(value.left.replace('px', ''));
    const y = parseInt(value.top.replace('px', ''));
    
    // Create or get student
    let studentId: string | undefined = undefined;
    if (value.student && !value.student.startsWith('Place ')) {
      // Check if we already have this student
      let student = Array.from(studentsMap.values()).find(s => s.name === value.student);
      
      if (!student) {
        // Create new student
        student = {
          id: uuidv4(),
          name: value.student
        };
        studentsMap.set(student.id, student);
      }
      
      studentId = student.id;
    }
    
    // Create desk
    desks.push({
      id: uuidv4(),
      position: { x, y },
      studentId
    });
  });
  
  return {
    desks,
    students: Array.from(studentsMap.values())
  };
};

// Export classroom data to a JSON file
export const exportToJson = (desks: DeskItem[], students: Student[]): void => {
  const data = convertToExportFormat(desks, students);
  const jsonString = JSON.stringify(data, null, 2);
  const blob = new Blob([jsonString], { type: 'application/json' });
  const url = URL.createObjectURL(blob);
  
  // Create a temporary link and trigger download
  const a = document.createElement('a');
  a.href = url;
  a.download = 'classroom-layout.json';
  document.body.appendChild(a);
  a.click();
  
  // Clean up
  document.body.removeChild(a);
  URL.revokeObjectURL(url);
};

// Import classroom data from a JSON file
export const importFromJson = (file: File): Promise<{ desks: DeskItem[], students: Student[] }> => {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    
    reader.onload = (event) => {
      try {
        const jsonData = JSON.parse(event.target?.result as string) as ImportFormat;
        const result = convertFromImportFormat(jsonData);
        resolve(result);
      } catch (error) {
        reject(new Error('Invalid JSON file format'));
      }
    };
    
    reader.onerror = () => {
      reject(new Error('Error reading file'));
    };
    
    reader.readAsText(file);
  });
}; 