import { DeskItem } from '../types/models';

// Desk dimensions
export const DESK_WIDTH = 24 * 4; // 24 tailwind units * 4px
export const DESK_HEIGHT = 16 * 4; // 16 tailwind units * 4px
export const DESK_SPACING = 10; // Minimum spacing between desks when snapped
export const MAGNETIC_RANGE = 20; // Range for magnetic snapping in pixels

// Check if two desks are colliding
export const isColliding = (desk1: DeskItem, desk2: DeskItem): boolean => {
  // Add a minimum spacing for collision detection
  const minSpacing = DESK_SPACING;
  
  return (
    desk1.position.x < desk2.position.x + DESK_WIDTH + minSpacing &&
    desk1.position.x + DESK_WIDTH + minSpacing > desk2.position.x &&
    desk1.position.y < desk2.position.y + DESK_HEIGHT + minSpacing &&
    desk1.position.y + DESK_HEIGHT + minSpacing > desk2.position.y
  );
};

// Find the next available position for a new desk
export const findNextAvailablePosition = (desks: DeskItem[]): { x: number; y: number } => {
  // Start from a reasonable position
  const startX = 100;
  const startY = 100;
  
  // If no desks, return the starting position
  if (desks.length === 0) {
    return { x: startX, y: startY };
  }
  
  // Grid-based approach to find the next available position
  const gridSize = DESK_WIDTH + 30; // Add some spacing for grid placement
  const maxCols = Math.floor((window.innerWidth - 300) / gridSize); // Accounting for control panel
  
  // Try positions in a grid pattern
  for (let row = 0; row < 100; row++) { // Limit to 100 rows to prevent infinite loop
    for (let col = 0; col < maxCols; col++) {
      const testPosition = {
        x: startX + col * gridSize,
        y: startY + row * gridSize
      };
      
      // Create a test desk
      const testDesk: DeskItem = {
        id: 'test',
        position: testPosition
      };
      
      // Check if this position collides with any existing desk
      const hasCollision = desks.some(desk => isColliding(testDesk, desk));
      
      if (!hasCollision) {
        return testPosition;
      }
    }
  }
  
  // If all positions are taken (unlikely), return a random position
  return {
    x: Math.floor(Math.random() * 400) + 100,
    y: Math.floor(Math.random() * 300) + 100
  };
};

// Find the nearest snap position for magnetic effect
export const findMagneticSnapPosition = (
  movingDesk: DeskItem,
  otherDesks: DeskItem[]
): { x: number; y: number } | null => {
  // Don't snap if there are no other desks
  if (otherDesks.length === 0) {
    return null;
  }
  
  // Minimum distance to trigger snapping
  let minDistance = MAGNETIC_RANGE;
  let snapPosition = null;
  
  // Check each desk for potential snapping
  for (const desk of otherDesks) {
    // Right edge of moving desk to left edge of other desk
    const rightToLeftDist = Math.abs((movingDesk.position.x + DESK_WIDTH) - desk.position.x);
    const rightToLeftVerticalAlign = Math.abs(movingDesk.position.y - desk.position.y);
    
    if (rightToLeftDist < minDistance && rightToLeftVerticalAlign < DESK_HEIGHT / 2) {
      minDistance = rightToLeftDist;
      snapPosition = { 
        x: desk.position.x - DESK_WIDTH - DESK_SPACING, 
        y: desk.position.y 
      };
    }
    
    // Left edge of moving desk to right edge of other desk
    const leftToRightDist = Math.abs(movingDesk.position.x - (desk.position.x + DESK_WIDTH));
    const leftToRightVerticalAlign = Math.abs(movingDesk.position.y - desk.position.y);
    
    if (leftToRightDist < minDistance && leftToRightVerticalAlign < DESK_HEIGHT / 2) {
      minDistance = leftToRightDist;
      snapPosition = { 
        x: desk.position.x + DESK_WIDTH + DESK_SPACING, 
        y: desk.position.y 
      };
    }
    
    // Bottom edge of moving desk to top edge of other desk
    const bottomToTopDist = Math.abs((movingDesk.position.y + DESK_HEIGHT) - desk.position.y);
    const bottomToTopHorizontalAlign = Math.abs(movingDesk.position.x - desk.position.x);
    
    if (bottomToTopDist < minDistance && bottomToTopHorizontalAlign < DESK_WIDTH / 2) {
      minDistance = bottomToTopDist;
      snapPosition = { 
        x: desk.position.x, 
        y: desk.position.y - DESK_HEIGHT - DESK_SPACING 
      };
    }
    
    // Top edge of moving desk to bottom edge of other desk
    const topToBottomDist = Math.abs(movingDesk.position.y - (desk.position.y + DESK_HEIGHT));
    const topToBottomHorizontalAlign = Math.abs(movingDesk.position.x - desk.position.x);
    
    if (topToBottomDist < minDistance && topToBottomHorizontalAlign < DESK_WIDTH / 2) {
      minDistance = topToBottomDist;
      snapPosition = { 
        x: desk.position.x, 
        y: desk.position.y + DESK_HEIGHT + DESK_SPACING 
      };
    }
  }
  
  return snapPosition;
}; 