export interface Student {
  id: string;
  name: string;
}

export interface DeskItem {
  id: string;
  position: { x: number; y: number };
  studentId?: string;
}

export type Mode = 'desk' | 'student'; 