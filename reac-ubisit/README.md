# Classroom Designer

A modern React application for designing classroom layouts. This application allows you to:

- Add desks to a virtual classroom
- Drag and position desks anywhere in the classroom
- Clear the classroom layout

## Features

- Interactive drag-and-drop interface
- Modern, minimalist UI design
- Responsive layout
- Visual representation of a classroom from above

## Technologies Used

- React
- TypeScript
- TailwindCSS
- React Draggable
- React Icons

## Getting Started

### Prerequisites

- Node.js (v14 or higher)
- npm or yarn

### Installation

1. Clone the repository
2. Navigate to the project directory
3. Install dependencies:

```bash
npm install
```

### Running the Application

To start the development server:

```bash
npm run dev
```

The application will be available at http://localhost:5173

### Building for Production

To build the application for production:

```bash
npm run build
```

## Usage

- Click "Add Desk" to add a new desk to the classroom
- Drag desks to position them as desired
- Use "Clear All" to remove all desks and start over

## License

MIT
