# UbiSit // plan de classe aleatoire



## Présentation

<iframe src="https://tube-numerique-educatif.apps.education.fr/w/3nzhE61Rn1TaX9W1vGxv6T" width="560" height="315" frameborder="0" allowfullscreen></iframe>

Ce site web permet aux professeurs de créer un plan de classe interactif dans un premier temps. Ils peuvent ensuite répartir les élèves aléatoirement ou non sur ce plan de classe.

Le projet est open-source et il respecte les normes RGPD, il ne stocke aucune donnée personnelle.
Les données sensibles comme les listes d'élèves ne quitte jamais l'ordinateur de l'utilisateur.

Vous pouvez accéder au site web à l'adresse suivante : [site officiel d'UbiSit](https://ubisit.forge.apps.education.fr/ubisit-plan-de-classe-aleatoire/)

## Technologies utilisées

Ce site web est développé en HTML, CSS et JavaScript. Il utilise la librairie [html2canvas](https://unpkg.com/html2canvas@1.0.0-rc.7/dist/html2canvas.js) pour générer des images à partir du plan de classe.

Il n'y a que très peu d'HTML et de CSS, la majorité du code est en JavaScript.

## UbiSit dans les médias

Article du café pédagogique : [Créer un plan numérique de classe](https://cafepedagogique.net/2024/09/13/creer-un-plan-numerique-de-classe/)

UbiSit présenté par un des utilisateurs : [UbiSit, une application libre pour gérer vos plans de classe](https://www.youtube.com/watch?v=ndVVH0MPbt8)
<iframe width="560" height="315" src="https://www.youtube.com/embed/ndVVH0MPbt8" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>