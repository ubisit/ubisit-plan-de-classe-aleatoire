var switchmode;
var addStudent;
var assignStudents;
var testcanva;
var showHelp;
var importStudentList;
var quickImportStudent;
var removeStudentFromPlanBtn;
var exportBureaus;
var displayOverlay;
var addBureauProfesseur;
var add;
var deleteAllBureaus;
var zoomOut;
var zoomIn;

document.addEventListener('DOMContentLoaded', function () {

  var xhr = new XMLHttpRequest();
  xhr.open('GET', 'https://ubisit.great-site.net/increment.php', true);
  xhr.send();

  const helpA = document.getElementById("helpA");
  const helpB = document.getElementById("helpB");
  const helpC = document.getElementById("helpC");
  const helpHoverAElements =
    document.getElementsByClassName("helpHoverA");

  const helpD = document.getElementById("helpD");
  const helpE = document.getElementById("helpE");
  const helpMDL = document.getElementById("helpMDL");

  const canardButton = document.getElementById("canard");
  const plehA = document.getElementById("plehA");
  const plehBA = document.getElementById("plehBA");
  const plehBB = document.getElementById("plehBB");
  const plehCA = document.getElementById("importBureausBtn");
  const plehCB = document.getElementById("exportBtn");
  const plehD = document.getElementById("removeStudentFromPlanBtn");
  const plehE = document.getElementById("importStudentListBtn");
  const plehG = document.getElementById("plehG");

  let zoom = 1;
  let fusible = true;

  let mode = "edition";
  var studentList = [];
  var bureauPositions = {};
  var studentPositions = {};
  var elementDragged = false;
  var isPressed = false;
  var helpStatus = false;


  function getQueryParams() {
    console.log("getQueryParams");
    const params = new URLSearchParams(window.location.search);
    console.log("params : " + params);
    const students = params.get('students');
    console.log("students : " + students);
    return students ? students.split(',') : [];
  }

  // Function to add students to the student list
  function addStudentsToList(students) {
    // Remove all student labels
    var studentLabels = document.getElementsByClassName("dadLabel");
    while (studentLabels.length > 0) {
      studentLabels[0].parentNode.removeChild(studentLabels[0]);
    }

    // Empty the student list
    studentList = [];

    // Clean up student names and add only new ones
    var cleanedStudents = students.map(name => name.replace(/[\[\]"]/g, '').trim());
    var newStudents = cleanedStudents.filter(name => !studentList.includes(name));
    processImportedNames(newStudents);

    // Update the student count
    updateStudentCount();
  }

  // Get students from URL and add them to the list
  const students = getQueryParams();
  if (students.length > 0) {
    addStudentsToList(students);
  }

  for (let i = 0; i < helpHoverAElements.length; i++) {
    helpHoverAElements[i].addEventListener("mouseover", () => {
      canardButton.classList.add("showMe");
    });
    helpHoverAElements[i].addEventListener("mouseout", () => {
      canardButton.classList.remove("showMe");
    });
  }

  var version = document.querySelector('meta[name="version"]').getAttribute('content');
  var versionElements = document.getElementsByClassName('version');
  for (var i = 0; i < versionElements.length; i++) {
    versionElements[i].innerText = version;
  }

  helpA.addEventListener("mouseover", () => {
    plehA.classList.add("showMe");
  });
  helpA.addEventListener("mouseout", () => {
    plehA.classList.remove("showMe");
  });

  helpB.addEventListener("mouseover", () => {
    switchToBureau();
    plehBA.classList.add("showMe");
    plehBB.classList.add("showMe");
  });
  helpB.addEventListener("mouseout", () => {
    plehBA.classList.remove("showMe");
    plehBB.classList.remove("showMe");
  });

  helpC.addEventListener("mouseover", () => {
    switchToBureau();
    plehCA.classList.add("showMe");
    plehCB.classList.add("showMe");
  });
  helpC.addEventListener("mouseout", () => {
    plehCA.classList.remove("showMe");
    plehCB.classList.remove("showMe");
  });

  helpD.addEventListener("mouseover", () => {
    switchToEleve();
    plehD.classList.add("showMe");
  });
  helpD.addEventListener("mouseout", () => {
    plehD.classList.remove("showMe");
  });

  helpE.addEventListener("mouseover", () => {
    switchToEleve();
    plehE.classList.add("showMe");
  });
  helpE.addEventListener("mouseout", () => {
    plehE.classList.remove("showMe");
  });

  helpMDL.addEventListener("mouseover", () => {
    switchToBureau();
    plehG.classList.add("showMe");
  });
  helpMDL.addEventListener("mouseout", () => {
    plehG.classList.remove("showMe");
  });

  document.getElementById('randomBureauBtn').addEventListener('click', function () {
    selectRandomBureau();
  });

  function selectRandomBureau() {
    removeHighlights();

    var bureaus = document.querySelectorAll('.bureau');
    var bureausWithStudents = Array.from(bureaus).filter(function (bureau) {
      return !bureau.querySelector('.bureau-label').innerText.includes('Place');
    });

    if (bureausWithStudents.length === 0) {
      alert('Il n\'y a pas d\'élèves à sélectionner');
      return;
    }

    let iterations = Math.floor(bureausWithStudents.length);
    let currentIteration = 0;
    const initialDelay = 1; // Start with a quick delay
    const finalDelay = 350; // End with a slow delay
    const delayIncrement = (finalDelay - initialDelay) / (iterations * 1.4);
    let currentDelay = initialDelay;

    let delays = [];
    const last5PercentIndex = Math.ceil(iterations * 0.9);
    const base = Math.pow((finalDelay / initialDelay), (1 / (iterations - last5PercentIndex)));

    for (let i = 0; i < iterations; i++) {
      if (i < last5PercentIndex) {
        delays.push(initialDelay + (delayIncrement * i));
        if (i > 0) {
          let cd = delays[i] - delays[i - 1];
          console.log('Delay 90 :' + i + ': ' + cd);
        }
      } else {
        const adjustedIndex = i - last5PercentIndex;
        delays.push(finalDelay / Math.pow(base, adjustedIndex));
        let cd = initialDelay - delays[i - 1];
        console.log('Delay 10 :' + i + ': ' + cd);
      }
    }

    function highlightNextBureau() {
      if (currentIteration < iterations) {
        removeHighlights();
        let randomIndex = Math.floor(Math.random() * bureausWithStudents.length);
        bureausWithStudents[randomIndex].classList.add('highlighted');
        console.log('Highlighting bureau ' + randomIndex + ' for ' + currentDelay + 'ms');
        setTimeout(() => {
          bureausWithStudents[randomIndex].classList.remove('highlighted');
          currentIteration++;
          //currentDelay = initialDelay + (delayIncrement * currentIteration);
          currentDelay = delays[currentIteration];
          if (currentIteration < iterations) {
            setTimeout(highlightNextBureau, currentDelay);
          } else {
            setTimeout(highlightNextBureau, currentDelay);
            finalizeSelection();
          }
        }, currentDelay);
      }
    }

    function finalizeSelection() {
      removeHighlights();
      let finalRandomIndex = Math.floor(Math.random() * bureausWithStudents.length);
      bureausWithStudents[finalRandomIndex].classList.add('highlighted');
      celebrateFinalBureauSelection(bureausWithStudents[finalRandomIndex]);
      // Move the alert inside a setTimeout to allow the UI to update
      setTimeout(() => {
        var bureauLabel = bureausWithStudents[finalRandomIndex].querySelector('.bureau-label');
        //alert('L\'élève ' + bureauLabel.innerText + ' a été tiré au sort');
      }, 0); // Execute immediately after the current call stack clears
    }

    highlightNextBureau();
  }

  function celebrateFinalBureauSelection(chosenBureau) {
    // Ensure the chosen bureau element is passed to the function
    if (!chosenBureau) {
      console.error('No chosen bureau provided for confetti celebration.');
      return;
    }

    // Get the chosen bureau's position relative to the viewport
    const rect = chosenBureau.getBoundingClientRect();
    const xPosition = (rect.left + rect.right) / 2;
    const yPosition = (rect.top + rect.bottom) / 2;

    // Convert position to a scale of 0 to 1
    const xOrigin = xPosition / window.innerWidth;
    const yOrigin = yPosition / window.innerHeight;

    // Create confetti effect at the chosen bureau's position
    confetti({
      particleCount: 100,
      spread: 70,
      origin: {x: xOrigin, y: yOrigin}
    });
  }

  function removeHighlights() {
    var bureaus = document.querySelectorAll('.bureau');
    var bureausWithStudents = Array.from(bureaus).filter(function (bureau) {
      return !bureau.querySelector('.bureau-label').innerText.includes('Place');
    });
    for (var i = 0; i < bureausWithStudents.length; i++) {
      bureausWithStudents[i].classList.remove('highlighted');
    }
  }

  const textContainer = document.getElementById("text-container");
  const body = document.body;
  const subFooteClone = document.getElementById("subFooterClone");
  const titlesplashscreen = document.getElementById(
    "title-splash-screen",
  );

  setTimeout(() => {
    textContainer.remove();
    subFooteClone.remove();
    titlesplashscreen.remove();
    body.classList.remove("welcome");
  }, 4000);

  const helpBtn = document.getElementById("helpBtn");


  helpBtn.addEventListener("click", () => {
    isPressed = !isPressed;
    if (isPressed) {
      helpBtn.classList.add("pressed");
    } else {
      helpBtn.classList.remove("pressed");
    }
  });

  const switchMonthly = document.getElementById("switchMonthly");
  const switchYearly = document.getElementById("switchYearly");


  if (switchMonthly.checked) {
    console.log("bureau");
  } else {
    console.log("eleve");

    setTimeout(() => {
      console.log("switching to eleve");
      switchMonthly.checked = true;
      switchYearly.checked = false;
    }, 500);
  }

  deleteAllBureaus = function () {
    var bureauElements = document.getElementsByClassName("bureau");
    if (bureauElements === 0) {
      alert("Il n'y a pas de place à supprimer");
    } else {
      if (confirm("Voulez-vous vraiment supprimer toutes les bureaux ?")) {
        while (bureauElements.length > 0) {
          bureauElements[0].parentNode.removeChild(bureauElements[0]);
        }
      }
    }
    updateBureauCount();
  }

  showHelp = function () {
    console.log("toggle class");
    var help = document.getElementById("helpCenter");
    help.classList.toggle("showHelp");
    help.classList.toggle("guide-container");
    var mainContainer = document.getElementById("mainContainer");
    if (helpStatus) {
      switchToBureau();
      helpStatus = false;
      mainContainer.style.borderRadius = "25px";
      mainContainer.style.borderLeft = "3px solid var(--mainColor)";

      mainContainer.style.width = mainContainer.offsetWidth + 3 + "px";
    } else {
      helpStatus = true;
      mainContainer.style.borderRadius = "0 25px 25px 0";
      mainContainer.style.borderLeft = "3px solid #f0f0f0";

      setTimeout(function () {
        mainContainer.style.borderLeft = "none";
        mainContainer.style.width = mainContainer.offsetWidth - 3 + "px";
      }, 200);
    }
  }

  var totalPlaces = document.getElementsByClassName("bureau").length;


  document
    .getElementById("studentName")
    .addEventListener("keydown", function (event) {
      if (event.keyCode === 13) {
        event.preventDefault();
        document.getElementById("newStudent").click();
      }
    });

  function createBureau(x, y, id) {
    var div = document.createElement("div");
    div.classList.add("bureau");
    div.classList.add("slowBureau");
    div.style.left = 0 + "px";
    div.style.top = 0 + "px";


    setTimeout(() => {
      div.style.left = x + "px";
      div.style.top = y + "px";

      setTimeout(() => {
        div.classList.remove("slowBureau");
      }, 1000);
    }, 10);
    div.id = id;

    var label = document.createElement("div");
    label.classList.add("bureau-label");
    label.innerText = "Place " + totalPlaces;
    div.appendChild(label);

    var deleteButton = createDeleteButton(div);
    div.appendChild(deleteButton);

    //document.body.appendChild(div);
    document.getElementById("bureauParent").appendChild(div);
    dragElement(div);

    totalPlaces++;
    updateBureauCount();
  }

  displayOverlay = function () {
    var modeleOverlay = document.querySelector(".modeleOverlay");
    console.log(modeleOverlay);
    var modeleOverlayDaddy = document.querySelector(".modeleOverlayDaddy");
    modeleOverlayDaddy.style.display = "flex";
    modeleOverlayDaddy.style.height = "100vh";

    modeleOverlayDaddy.style.zIndex = "10001";
    modeleOverlay.style.zIndex = "10002";
    modeleOverlayDaddy.style.opacity = "1";
    modeleOverlay.style.opacity = "1";

    document.body.style.overflow = "hidden";

    var deleteButton = document.createElement("button");
    deleteButton.innerHTML = "&#10006;";
    deleteButton.className = "delete-button";
    modeleOverlay.appendChild(deleteButton);
    deleteButton.style.opacity = "1";
    deleteButton.style.zIndex = "10003";
    deleteButton.style.right = "-2px";
    deleteButton.style.top = "-7px";
    deleteButton.style.boxShadow = "none";

    deleteButton.removeEventListener("click", function () {
      deleteBureau(bureauElement);
    });

    deleteButton.addEventListener("click", hideOverlay);

    var numRows = document.getElementById("numRows");
    var numCols = document.getElementById("numCols");
    var numWidth = document.getElementById("numWidth");

    function updatePreview() {
      var rows = parseInt(numRows.value);
      var cols = parseInt(numCols.value);
      var width = parseInt(numWidth.value);
      cols *= width;
      var colsPerGroup = cols;

      showPreview(rows, cols, width, colsPerGroup);
    }

    numRows.addEventListener("input", updatePreview);
    numCols.addEventListener("input", updatePreview);
    numWidth.addEventListener("input", updatePreview);

    var confirmButton = document.getElementById("confirmButton");
    confirmButton.addEventListener("click", function () {
      hideOverlay();

      var numRows = parseInt(document.getElementById("numRows").value);
      var numCols = parseInt(document.getElementById("numCols").value);
      var numWidth = parseInt(document.getElementById("numWidth").value);
      numCols *= numWidth;

      if (
        isNaN(numRows) ||
        isNaN(numCols) ||
        isNaN(numWidth) ||
        numRows < 1 ||
        numCols < 1 ||
        numWidth < 1
      ) {
        alert("Please enter valid dimensions.");
      } else {
        generateBureaus(numRows, numCols, numWidth);
      }
    });


    let clickOutsideListener = function (event) {
      if (!modeleOverlay.contains(event.target)) {
        console.log("hidden");
        hideOverlay();

      }
    };

    function hideOverlay() {
      modeleOverlayDaddy.style.zIndex = "-5";
      modeleOverlay.style.opacity = "0";
      modeleOverlayDaddy.style.opacity = "0";
      modeleOverlay.style.zIndex = "-5";
      setTimeout(() => {
        modeleOverlayDaddy.style.height = "auto";
        document.body.style.overflow = "auto";
      }, 301);
      document.removeEventListener("click", clickOutsideListener);
    }

    setTimeout(() => {
      document.addEventListener("click", clickOutsideListener);
    }, 301);
  }

  function showPreview(numRows, numCols, numWidth) {
    console.log("showPreview" + numRows + numCols + numWidth);
    var previewContainer = document.getElementById("previewContainer");
    previewContainer.innerHTML = "";

    document.getElementById("bureauCountPreview").innerHTML = (numCols / numWidth) * numWidth * numRows;
    var xOffset = 625 - (numWidth * numCols * 12) / 2;
    var yOffset = 30;
    var xSpacing = 10;
    var ySpacing = 30;
    var colsPerGroup = numWidth;
    var emptyColumnWidth = 15;

    for (var row = 0; row < numRows; row++) {
      for (var col = 0; col < numCols; col++) {
        var groupIndex = Math.floor(col / colsPerGroup);
        var x =
          xOffset +
          col * (xSpacing + emptyColumnWidth) +
          groupIndex * (xSpacing * colsPerGroup);
        var y = yOffset + row * ySpacing;

        var square = document.createElement("div");
        square.classList.add("preview-square");
        square.style.left = x + "px";
        square.style.top = y + "px";
        square.classList.add("green");
        previewContainer.appendChild(square);
      }
    }
  }

  function generateBureaus(numRows, numCols, numWidth) {
    removeAllBureaus();
    var xOffset = 0;
    var yOffset = 0;
    var xSpacing = 100;
    var ySpacing = 110;
    var bureausPerRow = numWidth;
    var id = 0;

    for (var row = 0; row < numRows; row++) {
      for (var col = 0; col < numCols; col++) {
        var groupIndex = Math.floor(col / bureausPerRow);
        var x = xOffset + col * xSpacing + groupIndex * (xSpacing / 2);
        var y = yOffset + row * ySpacing;
        id++;
        createBureau(x, y, id);
      }
    }
  }

  function modelePresentation() {
    console.log("Placing modele presentation");

    var importedPositions = {
      0: {left: "1px", top: "1px"},
      1: {left: "101px", top: "1px"},
      2: {left: "101px", top: "110px"},
      3: {left: "1px", top: "110px"},
      4: {left: "1px", top: "220px"},
      5: {left: "101px", top: "220px"},
      6: {left: "101px", top: "330px"},
      7: {left: "1px", top: "330px"},
      8: {left: "101px", top: "440px"},
      9: {left: "1px", top: "440px"},
      10: {left: "380px", top: "1px"},
      11: {left: "280px", top: "1px"},
      12: {left: "280px", top: "110px"},
      13: {left: "280px", top: "220px"},
      14: {left: "380px", top: "110px"},
      15: {left: "380px", top: "220px"},
      16: {left: "280px", top: "330px"},
      17: {left: "380px", top: "330px"},
      18: {left: "280px", top: "440px"},
      19: {left: "380px", top: "440px"},
      20: {left: "550px", top: "1px"},
      21: {left: "650px", top: "1px"},
      22: {left: "550px", top: "110px"},
      23: {left: "550px", top: "330px"},
      24: {left: "650px", top: "110px"},
      25: {left: "650px", top: "330px"},
      26: {left: "550px", top: "220px"},
      27: {left: "650px", top: "220px"},
      28: {left: "550px", top: "440px"},
      29: {left: "650px", top: "440px"},
    };

    var bureauElements = document.getElementsByClassName("bureau");
    while (bureauElements.length > 0) {
      bureauElements[0].parentNode.removeChild(bureauElements[0]);
    }

    for (var id in importedPositions) {
      var div = document.createElement("div");
      div.classList.add("bureau");
      div.id = id;
      div.style.left = importedPositions[id].left;
      div.style.top = importedPositions[id].top;


      var label = document.createElement("div");
      label.classList.add("bureau-label");
      var nb = parseInt(id) + 1;
      label.innerText = "Place " + nb;
      div.appendChild(label);


      var deleteButton = createDeleteButton(div);
      div.appendChild(deleteButton);

      //document.body.appendChild(div);
      document.getElementById("bureauParent").appendChild(div);

      dragElement(div);


      var currentId = parseInt(id);
      if (!isNaN(currentId) && currentId >= totalPlaces) {
        totalPlaces = currentId + 1;
      }
    }
    updateBureauCount();
  }

  zoomOut = function () {
    if (fusible) {
      fusible = false;
      alert("Pour tout zoom différent de 100%, la fonctionnalité 'bureau aimanté' sera désactivée.");
    }
    if (zoom >= 0.5) {
      zoom = zoom - 0.1;
      applyZoom();
    }
  }

  zoomIn = function () {
    if (fusible) {
      fusible = false;
      alert("Pour tout zoom différent de 100%, la fonctionnalité 'bureau aimanté' sera désactivée.");
    }
    if (zoom < 1.5) {
      zoom = zoom + 0.1;
      applyZoom();
    }
  }

  function applyZoom() {
    var bureauParent = document.getElementById("bureauParent");
    bureauParent.style.transform = "scale(" + zoom + ")";
    bureauParent.style.transformOrigin = "top left";
    var zoomLabel = document.getElementById("zoomLabel");
    zoomLabel.innerText = Math.round(zoom * 100) + "%";
  }


  var bureauElements = document.getElementsByClassName("bureau");
  var labelElements = document.getElementsByClassName("bureau-label");

  function updateBureauCount() {
    updateBureauPositions();
    totalPlaces = document.getElementsByClassName("bureau").length;
    var bureauCounts = document.getElementsByClassName("bureauCounts");

    var totalPlacesS = "Bureaux : " + totalPlaces;
    for (var i = 0; i < bureauCounts.length; i++) {
      bureauCounts[i].innerText = totalPlacesS;
    }
    moveMasterBureau();
    updateStudentCount();
  }


  function updateStudentCount() {

    var bureaulabel = document.getElementsByClassName("bureau-label");
    for (var i = 0; i < bureaulabel.length; i++) {
      if (
        bureaulabel[i].innerText.indexOf("Place") == -1 &&
        bureaulabel[i].innerText.indexOf("Professeur") == -1
      ) {
        if (studentList.indexOf(bureaulabel[i].innerText) == -1) {
          //studentList.push(bureaulabel[i].innerText);
        }
      }
    }
    var studentCounts = document.getElementsByClassName("studentCounts");
    for (var i = 0; i < studentCounts.length; i++) {
      console.log("studentCounts[i].innerText : " + studentCounts[i].innerText);
      studentCounts[i].innerText = "Élèves : " + studentList.length;
    }
    moveMasterBureau();
  }

  function moveMasterBureau() {
    return;
    setTimeout(() => {
      const masterBureau = document.querySelector(".bureauProfesseur");
      masterBureau.classList.add("slowBureau");
      var bureauElements = document.getElementsByClassName("bureau");
      console.log("bureauElements longueur : " + bureauElements.length);
      var minLeft = 100000;
      var maxLeft = 0;
      var minTop = 100000;
      var maxTop = 0;
      for (var i = 0; i < bureauElements.length; i++) {
        var bureau = bureauElements[i];
        var bureauLeft = parseInt(bureau.style.left);
        var bureauTop = parseInt(bureau.style.top);
        if (bureauLeft < minLeft) {
          minLeft = bureauLeft;
        }
        if (bureauLeft > maxLeft) {
          maxLeft = bureauLeft;
        }
        if (bureauTop < minTop) {
          minTop = bureauTop;
        }
        if (bureauTop > maxTop) {
          maxTop = bureauTop;
        }
      }
      console.log("minLeft : " + minLeft);
      console.log("maxLeft : " + maxLeft);
      console.log("minTop : " + minTop);
      console.log("maxTop : " + maxTop);


      masterBureau.style.top = maxTop + 100 + "px";
      setTimeout(() => {
        masterBureau.classList.remove("slowBureau");
      }, 1000);
    }, 10);

  }

  document
    .getElementById("importBureausBtn")
    .addEventListener("click", function () {
      document.getElementById("fileInput").click();
    });

  document
    .getElementById("importStudentListBtn")
    .addEventListener("click", function () {
      document.getElementById("fileInputList").value = "";
      document.getElementById("fileInputList").click();
    });


  function switchToBureau() {
    const bureauClickElement = document.getElementById("switchMonthly");
    bureauClickElement.click();
  }

  function switchToEleve() {
    const eleveClickElement = document.getElementById("switchYearly");
    eleveClickElement.click();
  }


  var bureauElements = document.getElementsByClassName("bureau");
  for (var i = 0; i < bureauElements.length; i++) {
    dragElement(bureauElements[i]);
  }


  var bureauProfesseurElements =
    document.getElementsByClassName("bureauProfesseur");
  for (var i = 0; i < bureauProfesseurElements.length; i++) {
    //dragElement(bureauProfesseurElements[i]);
    dragElementNoSnap(bureauProfesseurElements[i]);
  }

  add = function () {
    console.log("Adding a new bureau");

    var div = document.createElement("div");
    div.classList.add("bureau");
    document.getElementById("bureauParent").appendChild(div);


    var label = document.createElement("div");
    label.classList.add("bureau-label");
    label.innerText = "Place " + (totalPlaces + 1);
    div.appendChild(label);


    var deleteButton = createDeleteButton(div);
    div.appendChild(deleteButton);


    var newPosition = generateClosestPositionToTopLeft(div, "eleve");


    div.style.left = newPosition.x + "px";
    div.style.top = newPosition.y + "px";


    dragElement(div);

    totalPlaces++;
    updateBureauCount();
  }

  function generateClosestPositionToTopLeft(newBureau, type) {
    if (type === "professeur") {
      var minDistance = 210;
    } else {
      var minDistance = 110;
    }

    var topLeft = {x: 10, y: 10};
    var bestDistance = Number.MAX_VALUE;
    var bestPosition = topLeft;

    var existingBureaus = document.querySelectorAll(".bureau");
    var existingBureausProfesseur =
      document.querySelectorAll(".bureauProfesseur");
    var existingBureaus = Array.prototype.slice
      .call(existingBureaus)
      .concat(Array.prototype.slice.call(existingBureausProfesseur));

    for (
      var dx = 0;
      dx <= window.innerWidth - newBureau.offsetWidth;
      dx += minDistance
    ) {
      for (
        var dy = 0;
        dy <= window.innerHeight - newBureau.offsetHeight;
        dy += minDistance
      ) {
        var x = topLeft.x + dx;
        var y = topLeft.y + dy;

        var isOverlapping = false;

        for (var j = 0; j < existingBureaus.length; j++) {
          var existingBureau = existingBureaus[j];
          var existingRect = existingBureau.getBoundingClientRect();

          if (
            Math.abs(existingRect.left - x) < minDistance &&
            Math.abs(existingRect.top - y) < minDistance
          ) {
            isOverlapping = true;
            break;
          }
        }

        if (!isOverlapping) {

          var distanceToTopLeft = Math.sqrt(
            (x - topLeft.x) ** 2 + (y - topLeft.y) ** 2,
          );

          if (distanceToTopLeft < bestDistance) {
            bestDistance = distanceToTopLeft;
            bestPosition = {x: x, y: y};
          }
        }
      }
    }

    return bestPosition;
  }

  addBureauProfesseur = function () {
    console.log("Adding a new bureauProfesseur");

    var div = document.createElement("div");
    div.classList.add("bureauProfesseur");
    document.getElementById("bureauParent").appendChild(div);


    var label = document.createElement("div");
    label.classList.add("bureau-label-professeur");
    label.innerText = "Bureau Vide";
    div.appendChild(label);


    var deleteButton = createDeleteButton(div);
    div.appendChild(deleteButton);


    var newPosition = generateClosestPositionToTopLeft(div, "professeur");


    div.style.left = newPosition.x + "px";
    div.style.top = newPosition.y + "px";


    dragElement(div);

    totalPlaces++;
    updateBureauCount();
    updateBureauPositions();
  }

  var bureauElements = document.getElementsByClassName("bureau");
  for (var i = 0; i < bureauElements.length; i++) {
    var bureauElement = bureauElements[i];
    dragElement(bureauElement);


    var deleteButton = createDeleteButton();
    bureauElement.appendChild(deleteButton);


    deleteButton.addEventListener("click", function () {
      deleteBureau(bureauElement);
    });
  }


  function deleteBureau(bureauElement) {
    if (mode === "selection") {
      return;
    }


    bureauElement.remove();


    totalPlaces--;
    updateBureauCount();
    updateBureauPositions();
  }


  function createDeleteButton(bureauElement) {
    var deleteButton = document.createElement("button");
    deleteButton.innerHTML = "&#10006;";
    deleteButton.className = "delete-button";
    deleteButton.addEventListener("click", function () {
      deleteBureau(bureauElement);
    });
    return deleteButton;
  }

  function dragElement(elmnt) {
    if (mode != "selection") {
      var pos1 = 0,
        pos2 = 0,
        pos3 = 0,
        pos4 = 0;
      var label1 = document.getElementById("label1");
      if (document.getElementById(elmnt.id + "header")) {

        document.getElementById(elmnt.id + "header").onmousedown =
          dragMouseDown;
      } else {

        elmnt.onmousedown = dragMouseDown;
      }

      function dragMouseDown(e) {
        if (mode != "selection") {

          elmnt.style.zIndex = 100;

          elmnt.style.boxShadow = "5px 5px 5px 0px rgba(0,0,0,0.75)";

          elmnt.style.transform = "scale(1.05)";
          e = e || window.event;
          e.preventDefault();

          pos3 = e.clientX;
          pos4 = e.clientY;
          document.onmouseup = closeDragElement;

          document.onmousemove = elementDrag;
        }
      }


      function areBureausClose(bureau1, bureau2, snapDistance) {
        if (zoom != 1) {
          return;
        } else {

          var rect1 = bureau1.getBoundingClientRect();
          var rect2 = bureau2.getBoundingClientRect();


          var distanceHorizontal =
            Math.abs(rect1.left - rect2.left) - bureau1.offsetWidth;
          var distanceVertical =
            Math.abs(rect1.top - rect2.top) - bureau1.offsetHeight;

          return (
            distanceHorizontal <= snapDistance &&
            distanceVertical <= snapDistance
          );
        }
      }

      function elementDrag(e) {
        e = e || window.event;
        e.preventDefault();

        pos1 = pos3 - e.clientX;
        pos2 = pos4 - e.clientY;
        pos3 = e.clientX;
        pos4 = e.clientY;

        elmnt.style.top = elmnt.offsetTop - pos2 + "px";
        elmnt.style.left = elmnt.offsetLeft - pos1 + "px";

        var bureauRect = elmnt.getBoundingClientRect();
        var snapDistance = 5;
        var alignDistance = 5;
        var closestBureau = null;
        var closestDistanceHorizontal = snapDistance;
        var closestDistanceVertical = snapDistance;
        if (zoom != 1) {
          return;
        }
        for (var i = 0; i < bureauElements.length; i++) {
          var otherRect = bureauElements[i].getBoundingClientRect();
          if (bureauElements[i] !== elmnt) {
            var distanceHorizontal = Math.abs(
              bureauRect.left - otherRect.right,
            );
            if (distanceHorizontal < closestDistanceHorizontal) {
              closestDistanceHorizontal = distanceHorizontal;
              closestBureau = bureauElements[i];
            }
            distanceHorizontal = Math.abs(
              bureauRect.right - otherRect.left,
            );
            if (distanceHorizontal < closestDistanceHorizontal) {
              closestDistanceHorizontal = distanceHorizontal;
              closestBureau = bureauElements[i];
            }

            var distanceVertical = Math.abs(
              bureauRect.top - otherRect.bottom,
            );
            if (distanceVertical < closestDistanceVertical) {
              closestDistanceVertical = distanceVertical;
              closestBureau = bureauElements[i];
            }
            distanceVertical = Math.abs(bureauRect.bottom - otherRect.top);
            if (distanceVertical < closestDistanceVertical) {
              closestDistanceVertical = distanceVertical;
              closestBureau = bureauElements[i];
            }
          }
        }
        for (var i = 0; i < bureauElements.length; i++) {
          var bureau1 = elmnt;
          var bureau2 = bureauElements[i];

          if (
            bureau1 !== bureau2 &&
            areBureausClose(bureau1, bureau2, alignDistance)
          ) {

            if (
              Math.abs(bureau1.offsetTop - bureau2.offsetTop) <=
              alignDistance
            ) {
              elmnt.style.top = bureau2.offsetTop + "px";
            }


            if (
              Math.abs(bureau1.offsetLeft - bureau2.offsetLeft) <=
              alignDistance
            ) {
              elmnt.style.left = bureau2.offsetLeft + "px";
            }
          }
        }
        if (closestBureau) {
          var closestRect = closestBureau.getBoundingClientRect();
          if (closestDistanceHorizontal < closestDistanceVertical) {
            if (
              Math.abs(bureauRect.left - closestRect.right) < snapDistance
            ) {
              elmnt.style.left = closestRect.right + "px";
            }
            if (
              Math.abs(bureauRect.right - closestRect.left) < snapDistance
            ) {
              elmnt.style.left = closestRect.left - bureauRect.width + "px";
            }
          } else {
            if (
              Math.abs(bureauRect.top - closestRect.bottom) < snapDistance
            ) {
              elmnt.style.top = closestRect.bottom + "px";
            }
            if (
              Math.abs(bureauRect.bottom - closestRect.top) < snapDistance
            ) {
              elmnt.style.top = closestRect.top - bureauRect.height + "px";
            }
          }
        }
      }

      function closeDragElement() {

        elmnt.style.zIndex = 0;

        elmnt.style.boxShadow = "none";
        elmnt.style.transform = "scale(1)";

        document.onmouseup = null;
        document.onmousemove = null;
        updateBureauCount();
      }
    }
  }

  function dragElementNoSnap(elmnt) {
    if (mode != "selection") {
      var pos1 = 0,
        pos2 = 0,
        pos3 = 0,
        pos4 = 0;

      if (document.getElementById(elmnt.id + "header")) {
        document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
      } else {
        elmnt.onmousedown = dragMouseDown;
      }

      function dragMouseDown(e) {
        if (mode != "selection") {
          elmnt.style.zIndex = 100;
          elmnt.style.boxShadow = "5px 5px 5px 0px rgba(0,0,0,0.75)";
          e = e || window.event;
          e.preventDefault();
          pos3 = e.clientX;
          pos4 = e.clientY;
          document.onmouseup = closeDragElement;
          document.onmousemove = elementDrag;
        }
      }

      function elementDrag(e) {
        e = e || window.event;
        e.preventDefault();
        pos1 = pos3 - e.clientX;
        pos2 = pos4 - e.clientY;
        pos3 = e.clientX;
        pos4 = e.clientY;
        elmnt.style.top = elmnt.offsetTop - pos2 + "px";
        elmnt.style.left = elmnt.offsetLeft - pos1 + "px";
      }

      function closeDragElement() {
        elmnt.style.zIndex = 0;
        elmnt.style.boxShadow = "none";
        document.onmouseup = null;
        document.onmousemove = null;
        updateBureauCount();
      }
    }
  }

  assignStudents = function () {
    var availablePlaces = document.getElementsByClassName("bureau");

    if (studentList.length === 0) {
      alert("Il n'y a pas d'élèves à répartir.");

      var placedStudents = document.getElementsByClassName("bureau-label");
      for (var i = 0; i < placedStudents.length; i++) {
        if (placedStudents[i].innerText !== "Place vide") {
          studentList.push(placedStudents[i].innerText);
        }
      }
    }

    if (studentList.length > availablePlaces.length) {
      alert("Il n'y a pas assez de places pour tous les élèves.");
      return;
    }

    var placedStudents = document.getElementsByClassName("bureau-label");
    var nbStudentPlace = 0;
    var studentListCopy = [];
    studentListCopy = studentList.slice();
    var studentsToRemove = [];

    for (var i = 0; i < placedStudents.length; i++) {
      var labelContent = placedStudents[i].innerText;
      if (
        !labelContent.includes("Place") &&
        labelContent !== "Professeur"
      ) {
        nbStudentPlace++;
        studentsToRemove.push(labelContent);
      }
    }

    studentListCopy = studentListCopy.filter(
      (student) => !studentsToRemove.includes(student),
    );

    console.log("Students removed:", studentsToRemove);
    console.log("Updated studentListCopy:", studentListCopy);
    console.log("Original StudentList", studentList);


    console.log(
      "nbstudentPlacé : " +
      nbStudentPlace +
      " studentlist.length : " +
      studentList.length,
    );
    if (nbStudentPlace !== studentList.length) {
      console.log("0:10");
      console.log("compléter le plan");
      assignStudentsRandomly("complete", availablePlaces, studentListCopy);
    } else {
      studentListCopy = studentList;
      console.log("retirer les élèves et refaire une nouvelle disposition");

      for (var i = 0; i < placedStudents.length; i++) {
        if (placedStudents[i].innerText !== "Place vide") {
          placedStudents[i].innerText = "Place vide";
        }
      }
      assignStudentsRandomly("startOver", availablePlaces, studentListCopy);
    }

    updateBureauPositions();
    document.getElementById("studentListDiv").style.height = "50px";
  }


  function assignStudentsRandomly(mode, availablePlaces, studentListCopy) {
    console.log("Mode : " + mode);

    var dadLabels = document.getElementsByClassName("dadLabel");
    while (dadLabels.length > 0) {
      dadLabels = document.getElementsByClassName("dadLabel");
      dadLabels[0].parentNode.removeChild(dadLabels[0]);
    }

    var shuffledList = studentListCopy.slice();
    console.log("LIIIISSSTTTTEEE : " + studentListCopy);
    shuffleArray(shuffledList);
    var bureaus = document.querySelectorAll(".bureau");

    var unassignedBureaus = [];
    for (var i = 0; i < bureaus.length; i++) {
      var bureau = bureaus[i];
      var label = bureau.querySelector(".bureau-label");
      if (label.innerText.includes("Place")) {
        unassignedBureaus.push(bureau);
      }
    }
    shuffleArray(unassignedBureaus);

    if (unassignedBureaus.length < shuffledList.length) {
      alert("Il n'y a pas assez de places vides pour répartir les élèves.");
      return;
    }

    var studentIndex = 0;
    for (var i = 0; i < unassignedBureaus.length; i++) {
      var bureau = unassignedBureaus[i];
      var label = bureau.querySelector(".bureau-label");

      if (studentIndex < shuffledList.length) {
        label.innerText = shuffledList[studentIndex];
        studentIndex++;
      } else {
        break;
      }
    }
  }


  function shuffleArray(array) {
    for (var i = array.length - 1; i > 0; i--) {
      var j = Math.floor(Math.random() * (i + 1));
      var temp = array[i];
      array[i] = array[j];
      array[j] = temp;
    }
  }


  function getRandomColor() {
    var letters = "0123456789ABCDEF";
    var color = "#";
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }


  function getDistance(element1, element2) {
    var rect1 = element1.getBoundingClientRect();
    var rect2 = element2.getBoundingClientRect();
    var dx = rect1.left - rect2.left;
    var dy = rect1.top - rect2.top;
    return Math.sqrt(dx * dx + dy * dy);
  }


  function assignStudentsNextToEachOther() {
    var availablePlaces = document.getElementsByClassName("bureau");
    var shuffledStudents = studentList.slice();
    shuffledStudents.sort(() => Math.random() - 0.5);

    for (var i = 0; i < shuffledStudents.length; i++) {
      var student = shuffledStudents[i];
      var assignedPlace = null;
      var minDistance = 250;


      for (var j = 0; j < availablePlaces.length; j++) {
        var place = availablePlaces[j];
        var label = place.querySelector(".bureau-label");
        if (label.innerText) {
          var distance = getDistance(assignedPlace || place, place);
          console.log(
            "distance :" +
            distance +
            " minDistance :" +
            minDistance +
            " assignedPlace :" +
            assignedPlace +
            " place :" +
            place,
          );
          if (distance < minDistance) {
            assignedPlace = place;

          }
        }
      }


      if (assignedPlace) {
        var label = assignedPlace.querySelector(".bureau-label");
        label.innerText = student;
      } else {
        console.log("Not enough available bureaus for all students.");
        console.log(
          "Assigned " +
          i +
          " students out of " +
          shuffledStudents.length +
          " students.",
        );
        break;
      }
    }
  }


  function assignStudentsRandomlyBis() {
    if (studentList.length === 0) {
      alert("Il n'y a pas d'élèves à répartir.");
      return;
    }
    var availablePlaces = document.getElementsByClassName("bureau");
    if (studentList.length > availablePlaces.length) {
      alert("Il n'y a pas assez de places pour tous les élèves.");
      return;
    }

    for (var i = 0; i < availablePlaces.length; i++) {
      var label = availablePlaces[i].querySelector(".bureau-label");
      if (label.innerText.includes("Place")) {
        label.innerText = "Place vide";
        label.style.backgroundColor = "#f0f0f0";
        label.style.color = "#000";
      }
    }


    var hist = [];
    for (var i = 0; i < studentList.length; i++) {
      var randomIndex = Math.floor(Math.random() * availablePlaces.length);
      var free = availablePlaces[randomIndex]
        .querySelector(".bureau-label")
        .innerText.includes("Place");
      while (hist.includes(randomIndex) || !free) {
        randomIndex = Math.floor(Math.random() * availablePlaces.length);

        var bureauHeight =
          availablePlaces[randomIndex].getBoundingClientRect().top;
        var free = availablePlaces[randomIndex]
          .querySelector(".bureau-label")
          .innerText.includes("Place");
        console.log(
          "height :" +
          bureauHeight +
          "number" +
          randomIndex +
          "free" +
          free,
        );
      }
      hist.push(randomIndex);
      var label =
        availablePlaces[randomIndex].querySelector(".bureau-label");
      label.style.backgroundColor = "#f0f0f0";
      label.style.color = "#000";
      label.innerText = studentList[i];
      label.style.backgroundColor = "var(--mainColor)";
      label.style.color = "#fff";
      console.log(
        "assignation de " +
        studentList[i] +
        " à la place " +
        (randomIndex + 1),
      );
    }


    updateBureauPositions();
  }


  function updateStudentList() {
    var studentListDiv = document.getElementById("studentListDiv");
    studentListDiv.innerHTML = "";
    for (var i = 0; i < studentList.length; i++) {
      //studentListDiv.innerHTML += "<p>" + studentList[i] + "</p>";
    }
    updateLabelParkHeight();
    updateBureauPositions();
  }

  function updateLabelParkHeight() {
    var labelPark = document.getElementById("labelPark");
    var nbOfStudent = studentList.length;
    var height = 0;
    if (nbOfStudent > 0) {
      height = 9 + (37 * nbOfStudent) + 7;
    }
    document.getElementById("studentListDiv").style.height = height + "px";
  }

  exportBureaus = function () {

    var fileName = prompt(
      "Choisissez un nom pour votre sauvegarde de plan (par exemple le numéro de la salle pour pouvoir la partager avec vos collègues) :",
      "bureau_positions.json",
    );
    console.log("fileName : " + fileName);

    if (fileName === null || fileName.trim() === "") {
      alert("Nom de fichier invalide.");
      return;
    }


    if (!fileName.endsWith(".json")) {
      fileName += ".json";
    }


    updateBureauPositions();


    var exportData = {};


    var bureauElements = document.getElementsByClassName("bureau");
    console.log("Longueur liste bureau: " + bureauElements.length);
    var lastID = 0;
    for (var i = 0; i < bureauElements.length; i++) {
      var bureau = bureauElements[i];
      console.log(bureau);
      var id = bureau.id;
      if (id === "") {
        console.log("EXCEPTION");
        id = lastID + 1;
        id = id.toString();
      }
      console.log("id : " + id);
      var left = bureau.style.left;
      var top = bureau.style.top;


      var label = bureau.querySelector(".bureau-label");
      var studentName = label ? label.innerText : "";
      if (studentName === "") {
        studentName = "Place vide";
      }


      exportData[id] = {left: left, top: top, student: studentName};
      console.log("data : " + exportData);

      //convert id to int
      lastID = parseInt(id);
    }


    var jsonString = JSON.stringify(exportData);

    console.log("jsonString : " + jsonString);


    var blob = new Blob([jsonString], {type: "application/json"});
    var url = URL.createObjectURL(blob);
    var a = document.createElement("a");
    a.href = url;


    a.download = fileName;


    document.body.appendChild(a);
    a.click();


    document.body.removeChild(a);
  }


  function selectFile() {
    var fileInput = document.getElementById("fileInput");
    fileInput.click();
  }


  importBureaus = function () {
    var fileInput = document.getElementById("fileInput");
    var file = fileInput.files[0];

    var reader = new FileReader();
    reader.onload = function (event) {
      var importedData = JSON.parse(event.target.result);


      var bureauElements = document.getElementsByClassName("bureau");
      while (bureauElements.length > 0) {
        bureauElements[0].parentNode.removeChild(bureauElements[0]);
      }


      for (var id in importedData) {
        var div = document.createElement("div");
        div.classList.add("bureau");
        div.id = id;
        div.style.left = importedData[id].left;
        div.style.top = importedData[id].top;


        var label = createLabelElement(id, importedData[id].student);
        div.appendChild(label);
        console.log("label : " + label.innerText);


        var deleteButton = createDeleteButton(div);
        div.appendChild(deleteButton);

        document.getElementById("bureauParent").appendChild(div);

        dragElement(div);


        var currentId = parseInt(id);
        if (!isNaN(currentId) && currentId >= totalPlaces) {
          totalPlaces = currentId + 1;
        }
      }
    };

    reader.readAsText(file);
    updateBureauCount();
    updateStudentCount();
    setTimeout(function () {
      updateBureauPositions();
      updateStudentCount();
      updateBureauCount();
    }, 10);

    if (mode != "edition") {
      switchToBureau();
    }
  }

  function updateStudentPositions() {
    var studentLabels = document.getElementsByClassName("bureau-label");
    for (var i = 0; i < studentLabels.length; i++) {
      var studentLabel = studentLabels[i];
      var studentName = studentLabel.innerText;
      var position = {
        left: parseInt(studentLabel.style.left),
        top: parseInt(studentLabel.style.top),
      };
      studentPositions[studentName] = position;
    }
  }


  function createBureauElement(id, left, top) {
    var div = document.createElement("div");
    div.classList.add("bureau");
    div.id = id;
    div.style.left = left;
    div.style.top = top;


    var label = createLabelElement(id);
    div.appendChild(label);


    var deleteButton = createDeleteButton(div);
    div.appendChild(deleteButton);

    document.getElementById("bureauParent").appendChild(div);

    dragElement(div);
  }


  function createLabelElement(id, studentName) {
    var label = document.createElement("div");
    label.classList.add("bureau-label");
    label.innerText = studentName
      ? studentName
      : "Place " + (parseInt(id) + 1);
    return label;
  }


  function removeAllBureaus() {
    var bureauElements = document.getElementsByClassName("bureau");
    while (bureauElements.length > 0) {
      bureauElements[0].remove();
    }
  }


  function positionBureaus(positions) {
    removeAllBureaus();
    totalPlaces = Object.keys(positions).length;

    for (var bureauId in positions) {
      var position = positions[bureauId];


      var div = document.createElement("div");
      div.classList.add("bureau");
      div.id = bureauId;
      div.style.left = position.left;
      div.style.top = position.top;
      document.getElementById("bureauParent").appendChild(div);


      var label = document.createElement("div");
      label.classList.add("bureau-label");
      label.innerText = "Place " + bureauId.split("_")[1];
      div.appendChild(label);


      var deleteButton = createDeleteButton(div);
      div.appendChild(deleteButton);


      dragElement(div);
    }
  }

  function importStudentListBIS() {
    var fileInputList = document.getElementById("fileInputList");
    var file = fileInputList.files[0];

    var reader = new FileReader();
    reader.onload = function (event) {
      var importedData = event.target.result;


      if (file.name.endsWith(".csv")) {
        var importedNames = importedData
          .split("\n")
          .map((row) => {
            var cells = row.split(",");
            if (cells.length >= 2) {
              return cells
                .slice(0, 2)
                .map((cell) => cell.trim())
                .join(" ");
            }
          })
          .filter((name) => name !== undefined && name !== "");

        processImportedNames(importedNames);
      } else if (file.name.endsWith(".xls")) {
        var workbook = XLSX.read(importedData, {type: "binary"});
        var sheet = workbook.Sheets[workbook.SheetNames[0]];
        var importedNames = XLSX.utils.sheet_to_json(sheet).map((row) => {
          return row["Column1"] + " " + row["Column2"];
        });
        processImportedNames(importedNames);
      } else {
        var importedNames = importedData
          .split("\n")
          .map((name) => name.trim())
          .filter((name) => name !== "");

        processImportedNames(importedNames);
      }
    };

    if (file.name.endsWith(".txt")) {
      reader.readAsText(file, "text/plain; charset=utf-8");
    } else {
      reader.readAsBinaryString(file);
    }
  }

  quickImportStudent = function () {
    console.log("quickImportStudent");
    var modeleOverlay = document.querySelector(".popupStudentList");
    var modeleOverlayDaddy = document.querySelector(".popupStudentListDaddy");
    var studentListTextarea = document.getElementById("studentList");
    var studentCountPreview = document.getElementById("studentCountPreview");
    var confirmButton = document.getElementById("confirmButtonPopup");
    var quickAddButton = document.getElementById("quickAddButton");

    if (!modeleOverlay || !modeleOverlayDaddy || !studentListTextarea || !studentCountPreview || !confirmButton || !quickAddButton) {
      console.error("One or more elements are not found.");
      return;
    }

    modeleOverlayDaddy.style.display = "flex";
    modeleOverlayDaddy.style.height = "100vh";
    modeleOverlayDaddy.style.zIndex = "10001";
    modeleOverlay.style.zIndex = "10002";
    modeleOverlayDaddy.style.opacity = "1";
    modeleOverlay.style.opacity = "1";
    document.body.style.overflow = "hidden";

    var deleteButton = document.createElement("button");
    deleteButton.innerHTML = "&#10006;";
    deleteButton.className = "delete-button";
    modeleOverlay.appendChild(deleteButton);
    deleteButton.style.opacity = "1";
    deleteButton.style.zIndex = "10003";
    deleteButton.style.right = "-2px";
    deleteButton.style.top = "-7px";
    deleteButton.style.boxShadow = "none";
    deleteButton.addEventListener("click", hideOverlay);

    studentListTextarea.addEventListener("input", function () {
      var studentCount = studentListTextarea.value.split('\n').filter(line => line.trim() !== "").length;
      studentCountPreview.textContent = studentCount;
    });

    confirmButton.addEventListener("click", function () {
      hideOverlay();

      // Remove all student labels
      var studentLabels = document.getElementsByClassName("dadLabel");
      while (studentLabels.length > 0) {
        studentLabels[0].parentNode.removeChild(studentLabels[0]);
      }

      // Empty the student list
      studentList = [];

      var studentNames = studentListTextarea.value
        .split('\n')
        .map(name => name.trim())
        .filter(name => name !== "");

      // Check for existing students and add only new ones
      var newStudents = studentNames.filter(name => !studentList.includes(name));
      processImportedNames(newStudents);
      quickAddButton.textContent = "Éditer la liste";
    });

    if (quickAddButton.textContent === "Éditer la liste") {
      confirmButton.textContent = "Éditer la liste";
      studentListTextarea.value = studentList.join('\n');
      var studentCount = studentListTextarea.value.split('\n').filter(line => line.trim() !== "").length;
      studentCountPreview.textContent = studentCount;
    }

    function hideOverlay() {
      modeleOverlayDaddy.style.zIndex = "-5";
      modeleOverlay.style.opacity = "0";
      modeleOverlayDaddy.style.opacity = "0";
      modeleOverlay.style.zIndex = "-5";
      setTimeout(() => {
        modeleOverlayDaddy.style.height = "auto";
        document.body.style.overflow = "auto";
      }, 301);
      document.removeEventListener("click", clickOutsideListener);
    }

    let clickOutsideListener = function (event) {
      if (!modeleOverlay.contains(event.target)) {
        hideOverlay();
      }
    };

    setTimeout(() => {
      document.addEventListener("click", clickOutsideListener);
    }, 301);
  }

  importStudentList = function () {
    var fileInputList = document.getElementById("fileInputList");
    var file = fileInputList.files[0];

    var reader = new FileReader();
    reader.onload = function (event) {
      var importedData = event.target.result;


      var fileExtension = file.name.split(".").pop().toLowerCase();

      if (fileExtension === "csv") {
        var importedNames = importedData
          .split("\n")
          .map((row) => {
            var cells = row.split(",");
            if (cells.length >= 2) {
              return cells
                .slice(0, 2)
                .map((cell) => cell.trim())
                .join(" ");
            }
          })
          .filter((name) => name !== undefined && name !== "");

        processImportedNames(importedNames);
      } else if (fileExtension === "xls" || fileExtension === "xlsx") {

        var dataBuffer = new ArrayBuffer(importedData.length);
        var dataView = new Uint8Array(dataBuffer);
        for (var i = 0; i < importedData.length; i++) {
          dataView[i] = importedData.charCodeAt(i) & 0xff;
        }

        try {
          var workbook = XLSX.read(dataBuffer, {type: "array"});
          var sheet = workbook.Sheets[workbook.SheetNames[0]];
          var importedNames = XLSX.utils.sheet_to_json(sheet).map((row) => {
            return row["Column1"] + " " + row["Column2"];
          });
          processImportedNames(importedNames);
        } catch (error) {
          console.error("Error reading Excel file:", error);
        }
      } else {
        var importedNames = importedData
          .split("\n")
          .map((name) => name.trim())
          .filter((name) => name !== "");

        processImportedNames(importedNames);
      }
    };

    reader.readAsText(fileInputList.files[0], "utf-8");
  }

  function processImportedNames(importedNames) {
    studentList = importedNames;
    for (var i = 0; i < studentList.length; i++) {
      var studentName = studentList[i];
      var studentDiv = document.createElement("label");
      studentDiv.className = "dadLabel";
      studentDiv.innerText = studentName;
      studentDiv.setAttribute("draggable", "true");
      studentDiv.style.cursor = "move";
      studentDiv.style.zIndex = "1000";
      dragLabel(studentDiv);
      document.getElementById("labelPark").appendChild(studentDiv);
      positionLabels();
    }
    updateStudentList();
    updateStudentCount();
  }

  function positionLabels(offset) {
    var labels = document.getElementsByClassName("dadLabel");
    for (var i = 0; i < labels.length; i++) {
      labels[i].classList.add("animate-label");
    }

    if (typeof offset === "undefined") {
      offset = 9;
    }

    for (var i = 0; i < labels.length; i++) {
      var label = labels[i];
      label.style.position = "absolute";
      label.style.top = offset + "px";


      var computedStyle = window.getComputedStyle(label);
      var labelHeight = parseFloat(computedStyle.height);

      offset += labelHeight + 9;
      console.log("offset : " + offset);
    }
    setTimeout(function () {
      for (var i = 0; i < labels.length; i++) {
        labels[i].classList.remove("animate-label");
      }
    }, 500);

    syncDivPositions();
  }

  removeStudentFromPlanBtn = function () {
    var labels = document.getElementsByClassName("bureau-label");
    if (labels.length === 0) {
      alert("Il n'y a pas d'élèves à retirer.");
      return;
    } else {
      if (confirm("Voulez-vous vraiment retirer tous les élèves du plan ?")) {
        for (var i = 0; i < labels.length; i++) {

          if (labels[i].innerText != "Professeur") {
            var studentName = labels[i].innerText;
            var index = studentList.indexOf(studentName);
            if (index > -1) {
              studentList.splice(index, 1);
            }
            labels[i].innerText = "Place vide";
            labels[i].style.backgroundColor = "#f0f0f0";
            labels[i].style.color = "#000";

          }
        }
        updateBureauPositions();
      }
    }
  }


  testcanva = function () {
    switchToBureau();

    var fileName = prompt(
      "Choisissez un nom pour votre plan de classe :",
      "plan_capture.png",
    );

    if (fileName === null || fileName.trim() === "") {
      alert("Nom de fichier invalide");
      return;
    }


    if (!fileName.endsWith(".png")) {
      fileName += ".png";
    }

    console.log("screenshotBtn clicked");

    var help = document.getElementById("helpCenter");
    if (help.classList.contains("showHelp")) {
      showHelp();
    }


    var optionCenter = document.getElementById("optionCenter");
    optionCenter.style.display = "none";
    var helpCenter = document.getElementById("helpCenter");
    helpCenter.style.display = "none";
    var subfooter = document.getElementById("subFooter");
    subfooter.style.opacity = "0";

    var deleteButtons = document.getElementsByClassName("delete-button");
    for (var i = 0; i < deleteButtons.length; i++) {
      deleteButtons[i].style.display = "none";
    }

    var modeleOverlay = document.querySelector(".modeleOverlay");
    modeleOverlay.style.display = "none";
    var modeleOverlayDaddy = document.querySelector(".modeleOverlayDaddy");
    modeleOverlayDaddy.style.display = "none";


    document.body.classList.remove("welcome");


    document.body.classList.remove("scrollable-container");

    html2canvas(document.body).then(function (canvas) {

      var desiredWidth = 400;
      var desiredHeight = 800;


      var bureauElements = document.getElementsByClassName("bureau");
      var bureauProfesseurElements =
        document.getElementsByClassName("bureauProfesseur");

      bureauElements = Array.prototype.slice
        .call(bureauElements)
        .concat(Array.prototype.slice.call(bureauProfesseurElements));
      var minLeft = 100000;
      var maxLeft = 0;
      var minTop = 100000;
      var maxTop = 0;
      for (var i = 0; i < bureauElements.length; i++) {
        var bureau = bureauElements[i];
        console.log("bureau : " + bureau);
        var bureauLeft = parseInt(bureau.style.left);
        var bureauTop = parseInt(bureau.style.top);
        console.log("bureauLeft : " + bureauLeft);
        console.log("bureauTop : " + bureauTop);
        if (bureauLeft < minLeft) {
          minLeft = bureauLeft;
        }
        if (bureauLeft > maxLeft) {
          maxLeft = bureauLeft;
        }
        if (bureauTop < minTop) {
          minTop = bureauTop;
        }
        if (bureauTop > maxTop) {
          maxTop = bureauTop;
        }
      }
      desiredWidth = maxLeft - minLeft + 200;
      desiredHeight = maxTop - minTop + 200;
      console.log("minLeft : " + minLeft);
      console.log("maxLeft : " + maxLeft);
      console.log("minTop : " + minTop);
      console.log("maxTop : " + maxTop);
      console.log("desiredWidth : " + desiredWidth);
      console.log("desiredHeight : " + desiredHeight);


      var div = document.createElement("div");
      div.id = "screenshotDimensions";
      div.style.left = minLeft;
      div.style.top = minTop;
      div.style.width = desiredWidth;
      div.style.height = desiredHeight;
      document.body.appendChild(div);
      document.getElementById("screenshotDimensions").style.display =
        "block";
      document.getElementById(
        "screenshotDimensions",
      ).style.backgroundColor = "red";
      document.getElementById("screenshotDimensions").style.opacity = "0.5";
      document.getElementById("screenshotDimensions").style.zIndex = "1000";
      document.getElementById("screenshotDimensions").style.position =
        "absolute";
      document.getElementById("screenshotDimensions").style.border =
        "1px solid black";
      document.getElementById("screenshotDimensions").style.width =
        desiredWidth + 200 + "px";
      document.getElementById("screenshotDimensions").style.height =
        desiredHeight + 200 + "px";

      document.getElementById("screenshotDimensions").style.display =
        "none";

      var croppedCanvas = document.createElement("canvas");
      croppedCanvas.width = desiredWidth + 200;
      croppedCanvas.height = desiredHeight + 100;


      var scaleX = canvas.width / window.innerWidth;
      var scaleY = canvas.height / window.innerHeight;


      var cropX = (canvas.width - desiredWidth * scaleX) / 2;
      var cropY = (canvas.height - desiredHeight * scaleY) / 2;

      cropX = minLeft;
      cropY = minTop;

      desiredWidth = desiredWidth + 200;
      desiredHeight = desiredHeight + 100;


      var ctx = croppedCanvas.getContext("2d");
      ctx.drawImage(
        canvas,
        cropX,
        cropY,
        desiredWidth,
        desiredHeight,
        0,
        0,
        desiredWidth,
        desiredHeight,
      );


      var overlayText = "Plan de classe généré avec UbiSit";
      ctx.fillStyle = "rgba(1, 1, 1, 0.8)";
      ctx.fillRect(
        10,
        croppedCanvas.height - 40,
        ctx.measureText(overlayText).width * 1.6,
        30,
      );
      ctx.fillStyle = "White";
      ctx.font = "15px Arial";
      ctx.fillText(overlayText, 20, croppedCanvas.height - 20);

      document.body.classList.add("scrollable-container");

      var downloadLink = document.createElement("a");
      downloadLink.href = croppedCanvas.toDataURL("image/png");

      var date = new Date();
      var dateString =
        date.getDate() +
        "-" +
        (date.getMonth() + 1) +
        "-" +
        date.getFullYear();
      downloadLink.download = fileName;


      document.body.appendChild(downloadLink);
      downloadLink.click();


      document.body.removeChild(downloadLink);
    });
    optionCenter.style.display = "block";
    helpCenter.style.display = "block";
    var deleteButtons = document.getElementsByClassName("delete-button");
    for (var i = 0; i < deleteButtons.length; i++) {
      deleteButtons[i].style.display = "block";
    }
    var modeleOverlay = document.querySelector(".modeleOverlay");
    modeleOverlay.style.display = "inline";
    var modeleOverlayDaddy = document.querySelector(".modeleOverlayDaddy");
    modeleOverlayDaddy.style.display = "inline";
    var subfooter = document.getElementById("subFooter");
    subfooter.style.opacity = "1";
    document.body.classList.add("scrollable-container");
  }

  switchmode = function () {
    syncDivPositions();
    var bureauElements = document.getElementsByClassName("bureau");
    var labelElements = document.getElementsByClassName("bureau-label");

    var container = document.querySelector(".container");


    if (mode === "edition") {
      mode = "selection";
      var containeredition = document.getElementById("container-edition");
      containeredition.style.transform = "translateY(-100%)";
      containeredition.style.opacity = "0";


      var containerselection = document.getElementById(
        "container-selection",
      );
      var labelPark = document.getElementById("labelPark");

      containerselection.style.transform = "translateY(0%)";
      labelPark.style.transform = "translateY(0%)";
      containerselection.style.opacity = "1";
      labelPark.style.opacity = "1";
      labelPark.style.display = "block";


      for (var i = 0; i < bureauElements.length; i++) {
        bureauElements[i].style.cursor = "default";
        bureauElements[i].classList.toggle("canBeMoved");


        var deleteButton =
          bureauElements[i].querySelector(".delete-button");
        if (deleteButton) {
          deleteButton.style.display = "none";
        }
      }
      for (var i = 0; i < labelElements.length; i++) {
        labelElements[i].style.cursor = "grab";
        labelElements[i].style.position = "absolute";
      }

      var bureauProfesseurElements =
        document.getElementsByClassName("bureauProfesseur");
      for (var i = 0; i < bureauProfesseurElements.length; i++) {
        bureauProfesseurElements[i].style.cursor = "default";


        var deleteButton =
          bureauProfesseurElements[i].querySelector(".delete-button");
        if (deleteButton) {
          deleteButton.style.display = "none";
        }
      }
      var dadlabels = document.getElementsByClassName("dadLabel");

      for (var i = 0; i < dadlabels.length; i++) {
        dadlabels[i].style.display = "flex";
      }
      console.log("switch to selection mode");
    } else {
      mode = "edition";
      var containerselection = document.getElementById(
        "container-selection",
      );
      var labelPark = document.getElementById("labelPark");

      containerselection.style.transform = "translateY(-100%)";
      labelPark.style.transform = "translateY(-100%)";

      containerselection.style.opacity = "0";
      labelPark.style.opacity = "0";
      labelPark.style.display = "none";
      labelPark.style.width = "256px";
      labelPark.style.minHeight = "50px";


      var containeredition = document.getElementById("container-edition");


      containeredition.style.transform = "translateY(0%)";

      containeredition.style.opacity = "1";


      for (var i = 0; i < bureauElements.length; i++) {
        bureauElements[i].style.cursor = "move";
        bureauElements[i].classList.toggle("canBeMoved");


        var deleteButton =
          bureauElements[i].querySelector(".delete-button");
        if (deleteButton) {
          deleteButton.style.display = "block";
        }
      }

      var bureauProfesseurElements =
        document.getElementsByClassName("bureauProfesseur");
      for (var i = 0; i < bureauProfesseurElements.length; i++) {
        bureauProfesseurElements[i].style.cursor = "move";


        var deleteButton =
          bureauProfesseurElements[i].querySelector(".delete-button");
        if (deleteButton) {
          deleteButton.style.display = "block";
        }
      }
      var dadlabels = document.getElementsByClassName("dadLabel");

      for (var i = 0; i < dadlabels.length; i++) {
        dadlabels[i].style.display = "none";
      }
      console.log("switch to edition mode");
    }
    syncDivPositions();

    if (mode === "selection") {

      for (var i = 0; i < bureauElements.length; i++) {
        bureauElements[i]
          .querySelector(".bureau-label")
          .addEventListener("dblclick", editLabel);
        dragLabelSwitch(bureauElements[i].querySelector(".bureau-label"));
      }

      for (var i = 0; i < bureauElements.length; i++) {
        bureauElements[i]
          .querySelector(".bureauProfesseur-label")
          .addEventListener("dblclick", editLabelBureauProfesseur());
      }
    } else {

      for (var i = 0; i < bureauElements.length; i++) {
        bureauElements[i]
          .querySelector(".bureau-label")
          .removeEventListener("dblclick", editLabel);
      }

      for (var i = 0; i < bureauElements.length; i++) {
        bureauElements[i]
          .querySelector(".bureauProfesseur-label")
          .removeEventListener("dblclick", editLabelBureauProfesseur());
      }
    }
  }

  function syncDivPositions() {
    var labelParkDiv = document.getElementById("labelPark");
    var studentListDiv = document.getElementById("studentListDiv");

    var studentListRect = studentListDiv.getBoundingClientRect();
    labelParkDiv.style.left = studentListRect.left + "px";

    //labelParkDiv.style.height = studentListRect.height + "px";
  }

  function editLabel(event) {
    if (!elementDragged) {

      var inputElement = document.createElement("input");

      inputElement.setAttribute("type", "text");
      inputElement.setAttribute("rows", "2");
      inputElement.setAttribute("cols", "20");
      inputElement.setAttribute("wrap", "hard");
      inputElement.setAttribute("maxlength", "50");
      var labelElement = event.target;


      labelElement.setAttribute(
        "data-original-text",
        labelElement.innerText,
      );


      inputElement.value = labelElement.innerText;


      labelElement.parentElement.replaceChild(inputElement, labelElement);


      inputElement.focus();


      inputElement.addEventListener("keydown", function (event) {
        if (event.key === "Enter") {

          inputElement.blur();

          var newLabel = document.createElement("div");
          newLabel.innerText = inputElement.value;
          newLabel.className = "bureau-label";
          inputElement.parentElement.replaceChild(newLabel, inputElement);

          newLabel.addEventListener("dblclick", editLabel);
          for (var i = 0; i < bureauElements.length; i++) {
            bureauElements[i]
              .querySelector(".bureau-label")
              .addEventListener("dblclick", editLabel);
            dragLabelSwitch(
              bureauElements[i].querySelector(".bureau-label"),
            );
            bureauElements[i].querySelector(".bureau-label").style.cursor =
              "grab";
          }
          if (!newLabel.innerText.includes("Place")) {
            newLabel.style.backgroundColor = "var(--mainColor)";
            newLabel.style.color = "white";
          }
        }
      });


      inputElement.addEventListener("blur", function () {

        inputElement.blur();

        var newLabel = document.createElement("div");
        newLabel.innerText = inputElement.value;
        newLabel.className = "bureau-label";
        inputElement.parentElement.replaceChild(newLabel, inputElement);

        newLabel.addEventListener("dblclick", editLabel);
        for (var i = 0; i < bureauElements.length; i++) {
          bureauElements[i]
            .querySelector(".bureau-label")
            .addEventListener("dblclick", editLabel);
          dragLabelSwitch(bureauElements[i].querySelector(".bureau-label"));
          bureauElements[i].querySelector(".bureau-label").style.cursor =
            "grab";
        }
        if (!newLabel.innerText.includes("Place")) {
          newLabel.style.backgroundColor = "var(--mainColor)";
          newLabel.style.color = "white";
        }
      });
    }
  }

  function dragLabelSwitch(elmnt) {
    if (mode != "edition") {
      elmnt.style.position = "absolute";
      var originalTop = elmnt.offsetTop;
      var originalLeft = elmnt.offsetLeft;

      var OlabelTop;
      var OlabelLeft;

      var pos1 = 0,
        pos2 = 0,
        pos3 = 0,
        pos4 = 0;

      if (document.getElementById(elmnt.id + "header")) {
        document.getElementById(elmnt.id + "header").onmousedown =
          dragMouseDown;
      } else {
        elmnt.onmousedown = dragMouseDown;
      }


      var elementDragged = false;

      function dragMouseDown(e) {
        if (mode != "edition") {
          elmnt.parentNode.style.zIndex = 150;
          elmnt.style.zIndex = 155;
          OlabelTop = elmnt.offsetTop;
          OlabelLeft = elmnt.offsetLeft;
          console.log(
            "Olabel psotion : " +
            elmnt.innerText +
            " " +
            OlabelTop +
            " " +
            OlabelLeft,
          );
          e = e || window.event;
          e.preventDefault();
          elmnt.classList.add("dragged");

          document.getElementById("bin").style.opacity = "0.5";
          pos3 = e.clientX + 30;
          pos4 = e.clientY;
          document.onmouseup = closeDragElement;
          document.onmousemove = elementDrag;
        }
      }

      function elementDrag(e) {
        e = e || window.event;
        e.preventDefault();
        pos1 = pos3 - e.clientX;
        pos2 = pos4 - e.clientY;
        pos3 = e.clientX;
        pos4 = e.clientY;
        elmnt.style.top = elmnt.offsetTop - pos2 + "px";
        elmnt.style.left = elmnt.offsetLeft - pos1 + "px";

        elmnt.style.boxShadow = "5px 5px 5px 0px rgba(0,0,0,0.75)";
        elmnt.style.transform = "scale(1.05)";


        elementDragged = true;


        var hoveredBureaus = document.querySelectorAll(".bureau");
        for (var i = 0; i < hoveredBureaus.length; i++) {
          var bureauRect = hoveredBureaus[i].getBoundingClientRect();
          if (
            e.clientX >= bureauRect.left &&
            e.clientX <= bureauRect.right &&
            e.clientY >= bureauRect.top &&
            e.clientY <= bureauRect.bottom
          ) {
            hoveredBureaus[i].classList.add("highlighted");
          } else {
            hoveredBureaus[i].classList.remove("highlighted");
          }
        }

        var binRect = document
          .getElementById("bin")
          .getBoundingClientRect();
        if (
          e.clientX >= binRect.left &&
          e.clientX <= binRect.right &&
          e.clientY >= binRect.top &&
          e.clientY <= binRect.bottom
        ) {
          document.getElementById("bin").style.opacity = "1";
          document.getElementById("bin").style.transform = "scale(1.05)";
        } else {
          document.getElementById("bin").style.opacity = "0.5";
          document.getElementById("bin").style.transform = "scale(1)";
        }
      }

      function closeDragElement(e) {
        elmnt.classList.add("animate-label");
        elmnt.parentNode.style.zIndex = 0;
        elmnt.style.zIndex = 0;
        document.onmouseup = null;
        document.onmousemove = null;
        elmnt.style.boxShadow = "none";
        elmnt.style.transform = "scale(1)";

        elmnt.classList.remove("dragged");

        if (elementDragged) {
          var draggedRect = elmnt.getBoundingClientRect();
          var labels = document.getElementsByClassName("bureau-label");
          var droppedOnLabel = false;

          var width;
          var height;

          for (var i = 0; i < labels.length; i++) {
            var labelRect = labels[i].parentElement.getBoundingClientRect();
            if (
              e.clientX >= labelRect.left &&
              e.clientX <= labelRect.right &&
              e.clientY >= labelRect.top &&
              e.clientY <= labelRect.bottom
            ) {
              if (elmnt !== labels[i]) {
                var NlabelTop = labels[i].offsetTop;
                var NlabelLeft = labels[i].offsetLeft;
                console.log(
                  "Nlabel psotion : " +
                  labels[i].innerText +
                  " " +
                  NlabelTop +
                  " " +
                  NlabelLeft,
                );

                var tempText = elmnt.innerText;
                elmnt.innerText = labels[i].innerText;
                labels[i].innerText = tempText;
                droppedOnLabel = true;
                width = elmnt.offsetWidth;
                height = elmnt.offsetHeight;
                break;
              }
            }
          }

          if (droppedOnLabel) {
            elmnt.style.top = NlabelTop + "px";
            elmnt.style.left = NlabelLeft + "px";
            labels[i].style.top = OlabelTop + "px";
            labels[i].style.left = OlabelLeft + "px";
            console.log(
              "dropped on OOOlabel dimension: " +
              OlabelTop +
              " " +
              OlabelLeft,
            );
            console.log(
              "dropped on NNNlabel dimension: " +
              NlabelTop +
              " " +
              NlabelLeft,
            );
          } else {

            elmnt.style.top = originalTop + "px";
            elmnt.style.left = originalLeft + "px";
          }


          var binRect = document
            .getElementById("bin")
            .getBoundingClientRect();
          if (
            e.clientX >= binRect.left &&
            e.clientX <= binRect.right &&
            e.clientY >= binRect.top &&
            e.clientY <= binRect.bottom
          ) {
            positionLabels();
            //elmnt.style.animation = "elementDestroy .3s ease-in-out";
            setTimeout(function () {
              //elmnt.remove();

              var index = studentList.indexOf(elmnt.innerText);
              elmnt.innerText = "Place vide";
              elmnt.style.backgroundColor = "#f0f0f0";
              elmnt.style.color = "#000";
              console.log("index : " + index);
              console.log("studentList BEFORE : " + studentList);
              if (index > -1) {
                studentList.splice(index, 1);
              }
              console.log("studentList AFTER : " + studentList);
            }, 300);
            console.log("remove highlight");
            var highlightedBureaus = document.querySelectorAll(
              ".bureau.highlighted",
            );
            for (var i = 0; i < highlightedBureaus.length; i++) {
              highlightedBureaus[i].classList.remove("highlighted");
            }


            setTimeout(function () {
              elementDragged = false;
            }, 2000);

            updateBureauPositions();
            positionLabels();
          } else {
            var offset =
              parseFloat(window.getComputedStyle(elmnt).height) + 18;
            var labels = document.getElementsByClassName("dadLabel");
            for (var i = 0; i < labels.length; i++) {
              labels[i].classList.add("animate-label");
            }
            for (var i = 0; i < labels.length; i++) {
              if (labels[i] !== elmnt) {
                labels[i].style.position = "absolute";
                labels[i].style.top = offset + "px";


                var computedStyle = window.getComputedStyle(labels[i]);
                var labelHeight = parseFloat(
                  computedStyle.getPropertyValue("height"),
                );

                offset += labelHeight + 9;
              } else {
                labels[i].style.position = "absolute";
                labels[i].style.top = 9 + "px";
                labels[i].style.left = originalLeft + 9 + "px";


                var computedStyle = window.getComputedStyle(labels[i]);
                var labelHeight = parseFloat(
                  computedStyle.getPropertyValue("height"),
                );


              }
            }

            setTimeout(function () {
              for (var i = 0; i < labels.length; i++) {
                labels[i].classList.remove("animate-label");
              }
            }, 500);
          }
          document.getElementById("bin").style.opacity = "0";


        }


        var highlightedBureaus = document.querySelectorAll(
          ".bureau.highlighted",
        );
        for (var i = 0; i < highlightedBureaus.length; i++) {
          highlightedBureaus[i].classList.remove("highlighted");
        }


        setTimeout(function () {
          elementDragged = false;
        }, 2000);
        setTimeout(function () {
          elmnt.classList.remove("animate-label");
        }, 500);

        updateBureauPositions();
      }
    }
  }

  function updateBureauPositions() {
    bureauPositions = {};

    var bureauElements = document.getElementsByClassName("bureau");
    for (var i = 0; i < bureauElements.length; i++) {
      var left = bureauElements[i].getBoundingClientRect().left;
      var top = bureauElements[i].getBoundingClientRect().top;
      bureauPositions[i] = {
        left: left + "px",
        top: top + "px",
      };
    }

    var bureauLabels = document.getElementsByClassName("bureau-label");
    for (var i = 0; i < bureauLabels.length; i++) {
      var bureauLabel = bureauLabels[i];
      if (bureauLabel.innerText.includes("Place")) {
        bureauLabel.style.backgroundColor = "#f0f0f0";
        bureauLabel.style.color = "#000";
      } else {
        bureauLabel.style.backgroundColor = "var(--mainColor)";
        bureauLabel.style.color = "#fff";
      }
    }
  }

  function editLabelBureauProfesseur(event) {

    var inputElement = document.createElement("input");

    inputElement.setAttribute("type", "text");
    inputElement.setAttribute("rows", "2");
    inputElement.setAttribute("cols", "20");
    inputElement.setAttribute("wrap", "hard");
    inputElement.setAttribute("maxlength", "50");
    var labelElement = event.target;


    labelElement.setAttribute("data-original-text", labelElement.innerText);


    inputElement.value = labelElement.innerText;


    labelElement.parentElement.replaceChild(inputElement, labelElement);


    inputElement.focus();


    inputElement.addEventListener("keydown", function (event) {
      if (event.key === "Enter") {

        inputElement.blur();

        var newLabel = document.createElement("div");
        newLabel.innerText = inputElement.value;
        newLabel.className = "bureauProfesseur-label";
        inputElement.parentElement.replaceChild(newLabel, inputElement);

        newLabel.addEventListener("dblclick", editLabelBureauProfesseur);
      }
    });


    inputElement.addEventListener("blur", function () {

      inputElement.blur();

      var newLabel = document.createElement("div");
      newLabel.innerText = inputElement.value;
      newLabel.className = "bureauProfesseur-label";
      inputElement.parentElement.replaceChild(newLabel, inputElement);

      newLabel.addEventListener("dblclick", editLabelBureauProfesseur);
    });
  }

  function dragLabel(elmnt) {
    if (mode == "edition") {
      return;
    } else {
      elmnt.style.position = "absolute";
      var originalTop = elmnt.offsetTop;
      var originalLeft = elmnt.offsetLeft;

      var pos1 = 0,
        pos2 = 0,
        pos3 = 0,
        pos4 = 0;
      var label1 = document.getElementById("label1");
      if (document.getElementById(elmnt.id + "header")) {

        document.getElementById(elmnt.id + "header").onmousedown =
          dragMouseDown;
      } else {

        elmnt.onmousedown = dragMouseDown;
      }


      var elementDragged = false;

      function dragMouseDown(e) {
        if (mode != "edition") {
          e = e || window.event;
          e.preventDefault();
          elmnt.classList.add("dragged");

          document.getElementById("bin").style.opacity = "0.5";
          pos3 = e.clientX + 30;
          pos4 = e.clientY;
          elmnt.style.boxShadow = "5px 5px 5px 0px rgba(0,0,0,0.75)";
          elmnt.style.transform = "scale(1.05)";
          document.onmouseup = closeDragElement;
          document.onmousemove = elementDrag;
        }
      }

      function elementDrag(e) {
        e = e || window.event;
        e.preventDefault();
        pos1 = pos3 - e.clientX;
        pos2 = pos4 - e.clientY;
        pos3 = e.clientX;
        pos4 = e.clientY;
        elmnt.style.top = elmnt.offsetTop - pos2 + "px";
        elmnt.style.left = elmnt.offsetLeft - pos1 + "px";


        elementDragged = true;


        var hoveredBureaus = document.querySelectorAll(".bureau");
        for (var i = 0; i < hoveredBureaus.length; i++) {
          var bureauRect = hoveredBureaus[i].getBoundingClientRect();
          if (
            e.clientX >= bureauRect.left &&
            e.clientX <= bureauRect.right &&
            e.clientY >= bureauRect.top &&
            e.clientY <= bureauRect.bottom
          ) {
            hoveredBureaus[i].classList.add("highlighted");
            console.log("add highlight");
          } else {
            hoveredBureaus[i].classList.remove("highlighted");
          }
        }

        var binRect = document
          .getElementById("bin")
          .getBoundingClientRect();
        if (
          e.clientX >= binRect.left &&
          e.clientX <= binRect.right &&
          e.clientY >= binRect.top &&
          e.clientY <= binRect.bottom
        ) {
          document.getElementById("bin").style.opacity = "1";
          document.getElementById("bin").style.transform = "scale(1.05)";
        } else {
          document.getElementById("bin").style.opacity = "0.5";
          document.getElementById("bin").style.transform = "scale(1)";
        }
      }

      function closeDragElement(e) {
        document.onmouseup = null;
        document.onmousemove = null;

        elmnt.classList.remove("dragged");
        elmnt.style.boxShadow = "none";
        elmnt.style.transform = "scale(1)";
        elmnt.style.width = "240px";

        if (elementDragged) {
          var draggedRect = elmnt.getBoundingClientRect();
          var labels = document.getElementsByClassName("bureau-label");
          var droppedOnLabel = false;

          for (var i = 0; i < labels.length; i++) {
            var labelRect = labels[i].parentElement.getBoundingClientRect();
            if (
              e.clientX >= labelRect.left &&
              e.clientX <= labelRect.right &&
              e.clientY >= labelRect.top &&
              e.clientY <= labelRect.bottom
            ) {
              if (elmnt !== labels[i]) {

                var tempText = elmnt.innerText;
                elmnt.innerText = labels[i].innerText;
                labels[i].innerText = tempText;
                droppedOnLabel = true;
                positionLabels();
                //remove the student from the list
                var index = studentList.indexOf(labels[i].innerText);
                console.log("index of student to be removed : " + index);
                if (index > -1) {
                  studentList.splice(index, 1);
                }
                updateStudentList();
                console.log("removed student from list : " + studentList);
                break;
              }
            }
          }


          var binRect = document
            .getElementById("bin")
            .getBoundingClientRect();
          if (
            e.clientX >= binRect.left &&
            e.clientX <= binRect.right &&
            e.clientY >= binRect.top &&
            e.clientY <= binRect.bottom
          ) {
            positionLabels();
            elmnt.style.animation = "elementDestroy .3s ease-in-out";
            setTimeout(function () {
              elmnt.remove();

              var index = studentList.indexOf(elmnt.innerText);
              if (index > -1) {
                studentList.splice(index, 1);
              }
            }, 300);
            console.log("remove highlight");
            var highlightedBureaus = document.querySelectorAll(
              ".bureau.highlighted",
            );
            for (var i = 0; i < highlightedBureaus.length; i++) {
              highlightedBureaus[i].classList.remove("highlighted");
            }


            setTimeout(function () {
              elementDragged = false;
            }, 2000);

            updateBureauPositions();
            positionLabels();
          } else {
            var offset =
              parseFloat(window.getComputedStyle(elmnt).height) + 18;
            var labels = document.getElementsByClassName("dadLabel");
            for (var i = 0; i < labels.length; i++) {
              labels[i].classList.add("animate-label");
            }
            for (var i = 0; i < labels.length; i++) {
              if (labels[i] !== elmnt) {
                labels[i].style.position = "absolute";
                labels[i].style.top = offset + "px";


                var computedStyle = window.getComputedStyle(labels[i]);
                var labelHeight = parseFloat(
                  computedStyle.getPropertyValue("height"),
                );

                offset += labelHeight + 9;
              } else {
                labels[i].style.position = "absolute";
                labels[i].style.top = 9 + "px";
                labels[i].style.left = originalLeft + 9 + "px";


                var computedStyle = window.getComputedStyle(labels[i]);
                var labelHeight = parseFloat(
                  computedStyle.getPropertyValue("height"),
                );


              }
            }

            setTimeout(function () {
              for (var i = 0; i < labels.length; i++) {
                labels[i].classList.remove("animate-label");
              }
            }, 500);
          }

          if (droppedOnLabel) {

            elmnt.remove();
            console.log("remove highlight");
            var highlightedBureaus = document.querySelectorAll(
              ".bureau.highlighted",
            );
            for (var i = 0; i < highlightedBureaus.length; i++) {
              highlightedBureaus[i].classList.remove("highlighted");
            }


            setTimeout(function () {
              elementDragged = false;
            }, 2000);

            updateBureauPositions();
            positionLabels();
          } else {
            var offset =
              parseFloat(window.getComputedStyle(elmnt).height) + 18;
            var labels = document.getElementsByClassName("dadLabel");
            for (var i = 0; i < labels.length; i++) {
              labels[i].classList.add("animate-label");
            }
            for (var i = 0; i < labels.length; i++) {
              if (labels[i] !== elmnt) {
                labels[i].style.position = "absolute";
                labels[i].style.top = offset + "px";


                var computedStyle = window.getComputedStyle(labels[i]);
                var labelHeight = parseFloat(
                  computedStyle.getPropertyValue("height"),
                );

                offset += labelHeight + 9;
              } else {
                labels[i].style.position = "absolute";
                labels[i].style.top = 9 + "px";
                labels[i].style.left = originalLeft + 9 + "px";
              }
            }

            setTimeout(function () {
              for (var i = 0; i < labels.length; i++) {
                labels[i].classList.remove("animate-label");
              }
            }, 500);
          }
          document.getElementById("bin").style.opacity = "0";
        }
      }


      console.log("remove highlight");
      var highlightedBureaus = document.querySelectorAll(
        ".bureau.highlighted",
      );
      for (var i = 0; i < highlightedBureaus.length; i++) {
        highlightedBureaus[i].classList.remove("highlighted");
      }


      setTimeout(function () {
        elementDragged = false;
      }, 2000);

      updateBureauPositions();
    }


    setTimeout(function () {
      updateStudentList();
      updateStudentCount();

      updateBureauPositions();
      syncDivPositions();
    }, 20);
  }

  addStudent = function () {
    var studentName = document.getElementById("studentName").value;
    if (studentName.trim() === "") {
      alert("Entrez un nom d'élève");
      return;
    }

    var studentDiv = document.createElement("label");
    studentDiv.className = "dadLabel";
    studentDiv.innerText = studentName;
    studentDiv.setAttribute("draggable", "true");
    studentDiv.style.cursor = "grab";
    studentDiv.style.zIndex = "1000";
    dragLabel(studentDiv);
    document.getElementById("labelPark").appendChild(studentDiv);


    studentList.push(studentName);
    document.getElementById("studentForm").reset();
    console.log("Studentlist :::::: " + studentList);

    updateStudentList();
    updateStudentCount();
    positionLabels();
  }

  modelePresentation();
  document.getElementById("canard").addEventListener("click", function () {
    this.classList.add("active");
  });
});

