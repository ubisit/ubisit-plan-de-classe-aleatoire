document.addEventListener('DOMContentLoaded', function () {
    const rotateIcon = document.querySelector('.bureauProfesseur .rotate-icon');
    const bureauProf = document.querySelector('.bureauProfesseur');
    let rotationAngle = 0;

    rotateIcon.addEventListener('click', function () {
        //add a class to make the rotation smooth and the remove it afterwords
        bureauProf.classList.add('transition-transform');
        console.log("before",rotationAngle);
        //rotationAngle = (rotationAngle + 90) % 360;
        console.log("after",rotationAngle);
        //just invert the width and height of the element
        let width = bureauProf.clientWidth + 8;
        console.log("width",width);
        let height = bureauProf.clientHeight + 8;
        console.log("height",height);
        bureauProf.style.width = `${height}px`;
        bureauProf.style.height = `${width}px`;
        //bureauProf.style.transform = `rotate(${rotationAngle}deg)`;
        setTimeout(() => {
            bureauProf.classList.remove('transition-transform');
        }
            , 500);
    });
});