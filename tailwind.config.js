/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./public/**/*.html"],
  theme: {
    extend: {
      container: false,
    },
  },
  plugins: [],
  corePlugins: {
    preflight: false,
  }
}

