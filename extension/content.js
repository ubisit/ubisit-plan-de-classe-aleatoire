// Function to extract student list from Pronote
function extractStudentList() {
  let students = [];
  let index = 0;
  let studentElement;

  // Loop through the student elements using the pattern in the XPath
  while (studentElement = document.evaluate(`//*[@id="GInterface.Instances[2].Instances[2]_0_${index}_div"]/div/div`, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue) {
    students.push(studentElement.textContent.trim());
    index++;
  }

  return students;
}

// Check if the student list has already been extracted
if (!window.studentListExtracted) {
  // Extract the student list
  const studentList = extractStudentList();

  // Store the student list in Chrome storage
  chrome.storage.local.set({ studentList: studentList }, () => {
    console.log('Student list saved ', studentList.length, ' :', studentList);

    // Encode the student list as a JSON string
    const encodedStudentList = encodeURIComponent(JSON.stringify(studentList));

    // Open UbiSit with the student list as a query parameter
    const ubisitUrl = `https://ubisit.forge.apps.education.fr/ubisit-plan-de-classe-aleatoire/?students=${encodedStudentList}`;
    chrome.runtime.sendMessage({ action: 'openTab', url: ubisitUrl });
  });

  // Mark that the student list has been extracted
  window.studentListExtracted = true;
}